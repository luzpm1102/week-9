import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Card } from '../components/Card';
import { Loading } from '../components/Loading';
import { stateInterface } from '../interfaces/Share';
import { STORIES, STORYIMAGE, ellipsify } from '../helpers/constants';
import { setStories } from '../redux/actions/storiesAction';
import { StoriesResponse } from '../interfaces/Stories';
import { usePagination } from '../hooks/usePagination';
import Pagination from '../components/Pagination';
import Filter from '../components/stories/Filter';
import { Link } from 'react-router-dom';

export const StoriesList = () => {
  const [update, setUpdate] = useState(false);
  const [error, setError] = useState(false);
  useEffect(() => {
    getStories();
    setUpdate(false);
    setError(false);
  }, [update]);

  const stories = useSelector(
    (state: stateInterface) => state.allStories.stories
  );

  const dispatch = useDispatch();
  const recoverHidden = () => {
    localStorage.removeItem('hidden');
    setUpdate(true);
  };

  const getStories = async (query?: string) => {
    const result: void | AxiosResponse<StoriesResponse> = await axios
      .get(query ? STORIES + query : STORIES)
      .catch((err) => {
        setError(true);
      });
    if (result) {
      dispatch(setStories(result.data.data.results));
    }
  };

  const itemsLimit = 6;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(itemsLimit);

  const display = stories
    .slice(indexOfTheFirstItem, indexOfTheLastItem)
    .map((story) => (
      <Link
        key={story.id}
        className='comics-list__item'
        to={`/story/${story.id}`}
      >
        <Card img={STORYIMAGE} title={ellipsify(story.title)} />
      </Link>
    ));

  return (
    <>
      <Filter />
      {error && <h3 className='error'>NOT FOUND</h3>}
      {stories.length <= 0 && !error && <Loading />}
      <div className='center'>
        <button className='home-button' onClick={recoverHidden}>
          RECOVER HIDDEN
        </button>
      </div>
      <div className='character-list'>{display}</div>
      <Pagination
        itemsPerPage={itemsPerPage}
        totalpages={stories.length}
        paginate={paginate}
        currentPage={currentPage}
      />
    </>
  );
};
