import React from 'react';
import { useNavigate } from 'react-router-dom';

export const Error = () => {
  const navigate = useNavigate();

  const onClick = () => navigate('/');
  return (
    <div className='flex-container'>
      <h2>NOT FOUND</h2>
      <button className='home-button' onClick={onClick}>
        BACK HOME
      </button>
    </div>
  );
};
