import axios, { AxiosResponse } from 'axios';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { SingleSerie } from '../components/series/SingleSerie';
import { searchById } from '../helpers/constants';
import { SeriesResponse } from '../interfaces/Series';
import { stateInterface } from '../interfaces/Share';
import { setSerie } from '../redux/actions/seriesAction';

export const SeriesDetail = () => {
  useEffect(() => {
    getSerie();
  }, []);
  const response = useSelector(
    (state: stateInterface) => state.allSeries.serie
  );

  const serie = response;
  const { id } = useParams();

  const dispatch = useDispatch();
  const getSerie = async () => {
    const result: void | AxiosResponse<SeriesResponse> = await axios
      .get(searchById('series', id ? id : '0'))
      .catch((err) => {
        console.log(err);
      });
    if (result) {
      dispatch(setSerie(result.data.data.results[0]));
    }
  };

  return <div>{serie && <SingleSerie serie={serie} />}</div>;
};
