import axios, { AxiosResponse } from 'axios';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { SingleStory } from '../components/stories/SingleStory';
import { searchById } from '../helpers/constants';
import { stateInterface } from '../interfaces/Share';
import { StoriesResponse } from '../interfaces/Stories';
import { setStory } from '../redux/actions/storiesAction';

export const StoryDetail = () => {
  useEffect(() => {
    getStory();
  }, []);
  const response = useSelector(
    (state: stateInterface) => state.allStories.story
  );

  const story = response;
  const { id } = useParams();

  const dispatch = useDispatch();
  const getStory = async () => {
    const result: void | AxiosResponse<StoriesResponse> = await axios
      .get(searchById('stories', id ? id : '0'))
      .catch((err) => {
        console.log(err);
      });
    if (result) {
      dispatch(setStory(result.data.data.results[0]));
    }
  };

  return <div>{story && <SingleStory story={story} />}</div>;
};
