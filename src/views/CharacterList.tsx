import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Card } from '../components/Card';
import Filter from '../components/characters/Filter';
import { Loading } from '../components/Loading';
import Pagination from '../components/Pagination';
import { CHARACTERS } from '../helpers/constants';
import { usePagination } from '../hooks/usePagination';
import { CharactersResponse } from '../interfaces/Character';
import { stateInterface } from '../interfaces/Share';
import { setCharacters } from '../redux/actions/charactersAction';

export const CharacterList = () => {
  const characters = useSelector(
    (state: stateInterface) => state.allCharacters.characters
  );
  const [error, setError] = useState(false);
  const [update, setUpdate] = useState(false);
  const itemsLimit = 6;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(itemsLimit);

  const display = characters
    .slice(indexOfTheFirstItem, indexOfTheLastItem)
    .map((character) => (
      <Link
        key={character.id}
        className='character-list__item'
        to={`/character/${character.id}`}
      >
        <Card
          img={`${character.thumbnail.path}.${character.thumbnail.extension}`}
          title={character.name}
        />
      </Link>
    ));

  const dispatch = useDispatch();

  const getCharacters = async (query?: string) => {
    const result: void | AxiosResponse<CharactersResponse> = await axios
      .get(query ? CHARACTERS + query : CHARACTERS)
      .catch((err) => {
        setError(true);
      });
    if (result) {
      if (result.data.data.total === 0) {
        setError(true);
      } else {
        setError(false);
      }
      dispatch(setCharacters(result.data.data.results));
    }
  };

  const recoverHidden = () => {
    localStorage.removeItem('hidden');
    setUpdate(true);
  };

  useEffect(() => {
    setUpdate(false);
    getCharacters();
    setError(false);
  }, [update]);

  return (
    <>
      <Filter getCharacters={getCharacters} />
      {error && <h3 className='error'>NOT FOUND</h3>}
      <div className='center'>
        <button className='home-button button-size' onClick={recoverHidden}>
          RECOVER HIDDEN
        </button>
      </div>
      {characters.length <= 0 && !error && <Loading />}
      <div className='character-list'>{display}</div>
      <Pagination
        itemsPerPage={itemsPerPage}
        totalpages={characters.length}
        paginate={paginate}
        currentPage={currentPage}
      />
    </>
  );
};
