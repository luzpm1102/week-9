import { rest } from 'msw';
import { mockedStories } from './responses/stories';
import {
  mockedFilterCharacterByName,
  mockedFilterByComic,
  mockedFilterByStory,
} from './responses/filterCharacter';
import { mockedCharacters } from './responses/characters';
import {
  mockedFilterComicsByTitle,
  mockedFilterComicsByFormat,
} from './responses/filerComics';
import {
  mockedComicDetail,
  mockedComicCharacters,
  mockedComicStories,
} from './responses/comicDetail';
import {
  mockedCharacterDetail,
  mockedCharacterComics,
  mockedCharacterStories,
} from './responses/characterDetail';
import {
  mockedStoryCharacters,
  mockedStoryComics,
  mockedStoryDetail,
} from './responses/storyDetail';
import { mockedComics } from './responses/comics';
import { mockedCharacterSeries } from './responses/characterDetail';
import {
  mockedSerieDetail,
  mockedSeriesComics,
  mockedSeriesStories,
  mockedSeriesCharacters,
} from './responses/seriesDetail';
export const handlers = [
  //mocked stories call
  rest.get('https://gateway.marvel.com/v1/public/stories', (req, res, ctx) => {
    return res(ctx.status(200), ctx.json(mockedStories));
  }),
  rest.get(
    'https://gateway.marvel.com/v1/public/stories/7',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedStoryDetail));
    }
  ),

  rest.get(
    'http://gateway.marvel.com/v1/public/stories/7/characters',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedStoryCharacters));
    }
  ),

  rest.get(
    'http://gateway.marvel.com/v1/public/stories/7/comics',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedStoryComics));
    }
  ),

  //mocked character call

  rest.get(
    'https://gateway.marvel.com/v1/public/characters',
    (req, res, ctx) => {
      const name = req.url.searchParams.get('nameStartsWith');
      const comic = req.url.searchParams.get('comics');
      const storie = req.url.searchParams.get('stories');
      if (name) {
        return res(ctx.status(200), ctx.json(mockedFilterCharacterByName));
      }
      if (comic) {
        return res(ctx.status(200), ctx.json(mockedFilterByComic));
      }
      if (storie) {
        return res(ctx.status(200), ctx.json(mockedFilterByStory));
      }
      return res(ctx.status(200), ctx.json(mockedCharacters));
    }
  ),

  rest.get(
    'https://gateway.marvel.com/v1/public/characters/1009629',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedCharacterDetail));
    }
  ),

  rest.get(
    'http://gateway.marvel.com/v1/public/characters/1009629/comics',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedCharacterComics));
    }
  ),

  rest.get(
    'http://gateway.marvel.com/v1/public/characters/1009629/stories',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedCharacterStories));
    }
  ),
  rest.get(
    'http://gateway.marvel.com/v1/public/characters/1009629/series',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedCharacterSeries));
    }
  ),

  //mocked comics call

  rest.get('https://gateway.marvel.com/v1/public/comics', (req, res, ctx) => {
    const title = req.url.searchParams.get('titleStartsWith');
    const format = req.url.searchParams.get('format');

    if (title) {
      return res(ctx.status(200), ctx.json(mockedFilterComicsByTitle));
    }
    if (format) {
      return res(ctx.status(200), ctx.json(mockedFilterComicsByFormat));
    }
    return res(ctx.status(200), ctx.json(mockedComics));
  }),

  rest.get(
    'https://gateway.marvel.com/v1/public/comics/1332',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedComicDetail));
    }
  ),

  rest.get(
    'http://gateway.marvel.com/v1/public/comics/1332/characters',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedComicCharacters));
    }
  ),

  rest.get(
    'http://gateway.marvel.com/v1/public/comics/1332/stories',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedComicStories));
    }
  ),

  //series mock

  rest.get(
    'https://gateway.marvel.com/v1/public/series/565',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedSerieDetail));
    }
  ),
  rest.get(
    'http://gateway.marvel.com/v1/public/series/565/characters',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedSeriesCharacters));
    }
  ),
  rest.get(
    'http://gateway.marvel.com/v1/public/series/565/stories',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedSeriesStories));
    }
  ),
  rest.get(
    'http://gateway.marvel.com/v1/public/series/565/comics',
    (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(mockedSeriesComics));
    }
  ),
];
