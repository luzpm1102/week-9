export const mockedStoryDetail = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '01f13b1aa31863f36b82c416a049b7b033242d03',
  data: {
    offset: 0,
    limit: 20,
    total: 1,
    count: 1,
    results: [
      {
        id: 7,
        title:
          'Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf',
        description: '',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/7',
        type: 'story',
        modified: '1969-12-31T19:00:00-0500',
        thumbnail: null,
        creators: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/7/creators',
          items: [],
          returned: 0,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/7/characters',
          items: [],
          returned: 0,
        },
        series: {
          available: 1,
          collectionURI: 'http://gateway.marvel.com/v1/public/stories/7/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/6',
              name: 'CAGE HC (2002)',
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI: 'http://gateway.marvel.com/v1/public/stories/7/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/941',
              name: 'CAGE HC (Hardcover)',
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI: 'http://gateway.marvel.com/v1/public/stories/7/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/941',
          name: 'CAGE HC (Hardcover)',
        },
      },
    ],
  },
};

export const mockedStoryCharacters = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '5b42f5316672948759fe9dca55bd3ecb52f38e82',
  data: {
    offset: 0,
    limit: 20,
    total: 4,
    count: 4,
    results: [
      {
        id: 1009337,
        name: 'Havok',
        description: '',
        modified: '2013-10-23T12:39:26-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/e0/5261659ebeaf8',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009337',
        comics: {
          available: 348,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009337/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17701',
              name: 'Age of Apocalypse: The Chosen (1995) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12725',
              name: 'Alpha Flight (1983) #61',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67716',
              name: 'Astonishing X-Men (2017) #13',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67717',
              name: 'Astonishing X-Men (2017) #14',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67718',
              name: 'Astonishing X-Men (2017) #15',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67719',
              name: 'Astonishing X-Men (2017) #16',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67720',
              name: 'Astonishing X-Men (2017) #17',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/45939',
              name: 'Cable and X-Force (2012) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/45951',
              name: 'Cable and X-Force (2012) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/45952',
              name: 'Cable and X-Force (2012) #10',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20731',
              name: 'CLANDESTINE CLASSIC PREMIERE HC (Hardcover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20603',
              name: 'Classic X-Men (1986) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20566',
              name: 'Classic X-Men (1986) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20567',
              name: 'Classic X-Men (1986) #13',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20589',
              name: 'Classic X-Men (1986) #33',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4072',
              name: 'Decimation: X-Men the Day After (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20374',
              name: 'Defenders (1972) #63',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/8555',
              name: 'Earth X (1999) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/8556',
              name: 'Earth X (1999) #6',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4241',
              name: 'EARTH X TPB [NEW PRINTING] (Trade Paperback)',
            },
          ],
          returned: 20,
        },
        series: {
          available: 96,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009337/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3614',
              name: 'Age of Apocalypse: The Chosen (1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
              name: 'Alpha Flight (1983 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/23262',
              name: 'Astonishing X-Men (2017 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/16907',
              name: 'Cable and X-Force (2012 - 2014)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3874',
              name: 'CLANDESTINE CLASSIC PREMIERE HC (2008)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3751',
              name: 'Classic X-Men (1986 - 1990)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/18864',
              name: 'Cyclops (2014 - 2015)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1643',
              name: 'Decimation: X-Men the Day After (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3743',
              name: 'Defenders (1972 - 1986)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/378',
              name: 'Earth X (1999 - 2000)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1806',
              name: 'EARTH X TPB [NEW PRINTING] (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/16287',
              name: 'Gambit (2012 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/27271',
              name: 'Havok & Wolverine: Meltdown (2019)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/30175',
              name: 'Hellions (2020 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2021',
              name: 'Incredible Hulk (1962 - 1999)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3289',
              name: 'Infinity War (2005)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2039',
              name: 'Marvel Comics Presents (1988 - 1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1440',
              name: 'MARVEL MASTERWORKS: THE UNCANNY X-MEN VOL. 2 HC (2005)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3460',
              name: 'MARVEL MASTERWORKS: THE UNCANNY X-MEN VOL. 6 HC (2008)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1689',
              name: 'Marvel Masterworks: The X-Men Vol.6 (2006)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 394,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009337/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/594',
              name: 'X-MEN (2004) #164',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/624',
              name: 'X-MEN (2004) #162',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/630',
              name: 'X-MEN (2004) #166',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/632',
              name: 'X-MEN (2004) #167',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/634',
              name: 'X-MEN (2004) #168',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/638',
              name: 'X-MEN (2004) #170',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/640',
              name: '1 of 4 - Bizarre Love Triangle',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/648',
              name: '1 of 2- Black Panther crossover',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/660',
              name: 'X-MEN (2004) #181',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/666',
              name: 'X-MEN (2004) #184',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/668',
              name: 'X-MEN (2004) #185',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/670',
              name: 'X-MEN (2004) #186',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/672',
              name: 'X-MEN (2004) #187',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/680',
              name: '4 of 6 - Supernovas',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/776',
              name: 'UNCANNY X-MEN (1963) #475',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/778',
              name: 'UNCANNY X-MEN (1963) #476',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/781',
              name: 'UNCANNY X-MEN (1963) #478',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/789',
              name: 'UNCANNY X-MEN (1963) #482',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1745',
              name: 'UNCANNY X-MEN (1963) #433',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1888',
              name: 'New Mutants (2003) #9',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        events: {
          available: 14,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009337/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/233',
              name: 'Atlantis Attacks',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/240',
              name: 'Days of Future Present',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/246',
              name: 'Evolutionary War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/249',
              name: 'Fatal Attractions',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/252',
              name: 'Inferno',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/299',
              name: 'Messiah CompleX',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/154',
              name: 'Onslaught',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/276',
              name: 'War of Kings',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/280',
              name: 'X-Tinction Agenda',
            },
          ],
          returned: 14,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1009337/havok?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Havok?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009337/havok?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1009399,
        name: 'Legion',
        description: '',
        modified: '2017-08-24T12:36:39-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/30/526547cc31b36',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009399',
        comics: {
          available: 94,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009399/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12754',
              name: 'Alpha Flight (1983) #88',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/65913',
              name: 'Legion (2018) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66285',
              name: 'Legion (2018) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66544',
              name: 'Legion (2018) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66914',
              name: 'Legion (2018) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67352',
              name: 'Legion (2018) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66037',
              name: 'Legion: Son of X Vol. 1 - Prodigal (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66362',
              name: 'Legion: Son of X Vol. 2 - Invasive Exotics (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/68788',
              name: 'Legion: Son of X Vol. 4 - For We Are Many (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/68787',
              name: 'Legion: Trauma (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24262',
              name: 'New Mutants (2009) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/27398',
              name: 'New Mutants (2009) #1 (BENJAMIN VARIANT)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/26051',
              name: 'New Mutants (2009) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/28600',
              name: 'New Mutants (2009) #2 (2ND PRINTING VARIANT)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/26052',
              name: 'New Mutants (2009) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/26053',
              name: 'New Mutants (2009) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/26054',
              name: 'New Mutants (2009) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/26058',
              name: 'New Mutants (2009) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/40170',
              name: 'New Mutants (2009) #23 (2nd Printing Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/36497',
              name: 'New Mutants (2009) #24',
            },
          ],
          returned: 20,
        },
        series: {
          available: 32,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009399/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
              name: 'Alpha Flight (1983 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/23908',
              name: 'Legion (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/23956',
              name: 'Legion: Son of X Vol. 1 - Prodigal (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24041',
              name: 'Legion: Son of X Vol. 2 - Invasive Exotics (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24966',
              name: 'Legion: Son of X Vol. 4 - For We Are Many (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24965',
              name: 'Legion: Trauma (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2055',
              name: 'New Mutants (1983 - 1991)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/7455',
              name: 'New Mutants (2009 - 2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24008',
              name: 'New Mutants by Zeb Wells: The Complete Collection (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/4603',
              name: 'New Mutants Classic Vol. 3 (2008)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13519',
              name: 'New Mutants Classic Vol. 6 (2010 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3694',
              name: 'Secret Wars II (1985)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2258',
              name: 'Uncanny X-Men (1963 - 2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/26038',
              name: 'Uncanny X-Men (2018 - 2019)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/31392',
              name: 'Way of X (2021 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3648',
              name: 'What If? (1989 - 1998)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2098',
              name: 'X-Factor (1986 - 1998)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/6689',
              name: 'X-Factor Annual (1986 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3633',
              name: 'X-Force (1991 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2265',
              name: 'X-Men (1991 - 2001)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 104,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009399/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/19864',
              name: 'Kings of Pain Part 3',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/19865',
              name: 'Kings of Pain Part 3: Queens of Sacrifice',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21350',
              name: 'Building Blocks Part Two: Trust',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21421',
              name: 'David Charles Haller',
              type: 'pin-up',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21465',
              name: 'NEW MUTANTS (1983) #44',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21466',
              name: 'Runaway!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22106',
              name: 'Promised Vengeance',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22107',
              name: 'Legion Quest',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22108',
              name: 'The Waking',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22112',
              name: 'Creatures On the Loose',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22327',
              name: 'Clash Reunion',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/24493',
              name: 'Endings',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/28017',
              name: 'To Save The Dream--A New Legend Is Born!',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/28018',
              name: 'All-New, All-Different--Here We Go Again',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/28020',
              name: 'Crash and Burn',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/28024',
              name: 'I Am Lady Mandarin',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/28026',
              name: 'Broken Chains',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/28028',
              name: 'Dream a Little Dream',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/28066',
              name: 'The Battle of Muir Isle',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/28070',
              name: 'One Step Back--Two Steps Forward',
              type: 'interiorStory',
            },
          ],
          returned: 20,
        },
        events: {
          available: 5,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009399/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/32',
              name: 'Kings of Pain',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/271',
              name: 'Secret Wars II',
            },
          ],
          returned: 5,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1009399/legion?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Legion_(David_Haller)?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009399/legion?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1009465,
        name: 'Mystique',
        description: '',
        modified: '2014-06-05T17:08:23-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/5/03/5390dc2225782',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009465',
        comics: {
          available: 197,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009465/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/72947',
              name: 'Age of X-Man: The Amazing Nightcrawler (2019) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/37996',
              name: 'Age of X: Alpha (2010) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38523',
              name: 'Age of X: Universe (2011) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43474',
              name: 'All-New X-Men (2012) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43475',
              name: 'All-New X-Men (2012) #10',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/64883',
              name: 'Astonishing X-Men (2017) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/65055',
              name: 'Astonishing X-Men (2017) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/65263',
              name: 'Astonishing X-Men (2017) #6',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66265',
              name: 'Astonishing X-Men (2017) #8',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66481',
              name: 'Astonishing X-Men (2017) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67715',
              name: 'Astonishing X-Men (2017) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/45823',
              name: 'Astonishing X-Men (2004) #62',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/45825',
              name: 'Astonishing X-Men (2004) #64',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/45826',
              name: 'Astonishing X-Men (2004) #65',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66299',
              name: 'Astonishing X-Men by Charles Soule Vol. 1: Life of X (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/7467',
              name: 'Cable (1993) #87',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/30059',
              name: 'Daken: Dark Wolverine (2010) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/28689',
              name: 'Dark X-Men (2009) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/68256',
              name: 'Death of Wolverine: The Complete Collection (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4072',
              name: 'Decimation: X-Men the Day After (Trade Paperback)',
            },
          ],
          returned: 20,
        },
        series: {
          available: 73,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009465/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/26332',
              name: 'Age of X-Man: The Amazing Nightcrawler (2019)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13603',
              name: 'Age of X: Alpha (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13896',
              name: 'Age of X: Universe (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/16449',
              name: 'All-New X-Men (2012 - 2015)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/744',
              name: 'Astonishing X-Men (2004 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/23262',
              name: 'Astonishing X-Men (2017 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24018',
              name: 'Astonishing X-Men by Charles Soule Vol. 1: Life of X (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1995',
              name: 'Cable (1993 - 2002)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9368',
              name: 'Daken: Dark Wolverine (2010 - 2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9051',
              name: 'Dark X-Men (2009)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24740',
              name: 'Death of Wolverine: The Complete Collection (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1643',
              name: 'Decimation: X-Men the Day After (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/31894',
              name: 'Demon Days: Cursed Web (2021)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/21593',
              name: 'Gambit (1999 - 2001)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2021',
              name: 'Incredible Hulk (1962 - 1999)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/32954',
              name: 'Inferno (2021 - 2022)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/18407',
              name: 'Magneto (2014 - 2015)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3719',
              name: 'Marvel Fanfare (1982 - 1992)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2301',
              name: 'Marvel Super Heroes (1990 - 1993)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1637',
              name: 'Marvel Visionaries: John Romita Jr. (2005)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 242,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009465/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/590',
              name: 'Interior #590',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/660',
              name: 'X-MEN (2004) #181',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/666',
              name: 'X-MEN (2004) #184',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/668',
              name: 'X-MEN (2004) #185',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/672',
              name: 'X-MEN (2004) #187',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/678',
              name: 'X-MEN (2004) #190',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/680',
              name: '4 of 6 - Supernovas',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/692',
              name: 'Cover #692',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/693',
              name: 'Interior #693',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1598',
              name: 'Cover #1598',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1599',
              name: 'Interior #1599',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1739',
              name: 'Interior #1739',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1905',
              name: '2 of 5 - Quiet',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1906',
              name: '2 of 5 - Quiet',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1916',
              name: 'Cover #1916',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1917',
              name: 'Interior #1917',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1918',
              name: 'Cover #1918',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1919',
              name: 'Interior #1919',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1920',
              name: 'Cover #1920',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1921',
              name: 'Interior #1921',
              type: 'interiorStory',
            },
          ],
          returned: 20,
        },
        events: {
          available: 10,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009465/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/320',
              name: 'Axis',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/318',
              name: 'Dark Reign',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/251',
              name: 'House of M',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/32',
              name: 'Kings of Pain',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/299',
              name: 'Messiah CompleX',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/263',
              name: 'Mutant Massacre',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/154',
              name: 'Onslaught',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/308',
              name: 'X-Men: Regenesis',
            },
          ],
          returned: 10,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/characters/1552/mystique?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Mystique?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009465/mystique?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1009499,
        name: 'Polaris',
        description: '',
        modified: '2013-11-01T16:43:39-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/c0/5274122562b05',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009499',
        comics: {
          available: 266,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009499/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/7455',
              name: 'Cable (1993) #76',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/7457',
              name: 'Cable (1993) #78',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20603',
              name: 'Classic X-Men (1986) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20566',
              name: 'Classic X-Men (1986) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20567',
              name: 'Classic X-Men (1986) #13',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20589',
              name: 'Classic X-Men (1986) #33',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4072',
              name: 'Decimation: X-Men the Day After (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20374',
              name: 'Defenders (1972) #63',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/20233',
              name: 'Doctor Strange, Sorcerer Supreme (1988) #69',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/8555',
              name: 'Earth X (1999) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/8556',
              name: 'Earth X (1999) #6',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4241',
              name: 'EARTH X TPB [NEW PRINTING] (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24259',
              name: 'Fantastic Force (2009) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/25161',
              name: 'Fantastic Force (2009) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/3463',
              name: 'House of M: Uncanny X-Men (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/9202',
              name: 'Incredible Hulk (1962) #391',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/9203',
              name: 'Incredible Hulk (1962) #392',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17457',
              name: 'Infinity War (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/52069',
              name: 'Magneto (2014) #20',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/52070',
              name: 'Magneto (2014) #21',
            },
          ],
          returned: 20,
        },
        series: {
          available: 69,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009499/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1995',
              name: 'Cable (1993 - 2002)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3751',
              name: 'Classic X-Men (1986 - 1990)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1643',
              name: 'Decimation: X-Men the Day After (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3743',
              name: 'Defenders (1972 - 1986)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3741',
              name: 'Doctor Strange, Sorcerer Supreme (1988 - 1996)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/378',
              name: 'Earth X (1999 - 2000)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1806',
              name: 'EARTH X TPB [NEW PRINTING] (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/7238',
              name: 'Fantastic Force (2009)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1412',
              name: 'House of M: Uncanny X-Men (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2021',
              name: 'Incredible Hulk (1962 - 1999)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3289',
              name: 'Infinity War (2005)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/18407',
              name: 'Magneto (2014 - 2015)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1440',
              name: 'MARVEL MASTERWORKS: THE UNCANNY X-MEN VOL. 2 HC (2005)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3460',
              name: 'MARVEL MASTERWORKS: THE UNCANNY X-MEN VOL. 6 HC (2008)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1595',
              name: 'Marvel Visionaries: Chris Claremont (2005)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/8132',
              name: 'Moon Knight Vol. 5: Down South (2009 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/8131',
              name: 'Moon Knight Vol. 5: Down South (2009 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/8133',
              name: 'Ms. Marvel Vol. 6: Ascension (2009 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/8135',
              name: 'Ms. Marvel Vol. 7: Dark Reign (2009 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/8134',
              name: 'Ms. Marvel Vol. 7: Dark Reign (2009 - Present)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 310,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009499/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/594',
              name: 'X-MEN (2004) #164',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/624',
              name: 'X-MEN (2004) #162',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/630',
              name: 'X-MEN (2004) #166',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/634',
              name: 'X-MEN (2004) #168',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/636',
              name: 'X-MEN (2004) #169',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/638',
              name: 'X-MEN (2004) #170',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/648',
              name: '1 of 2- Black Panther crossover',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/660',
              name: 'X-MEN (2004) #181',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/668',
              name: 'X-MEN (2004) #185',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/672',
              name: 'X-MEN (2004) #187',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/749',
              name: '1 of 4 - Season of the Witch',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/776',
              name: 'UNCANNY X-MEN (1963) #475',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/781',
              name: 'UNCANNY X-MEN (1963) #478',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/783',
              name: 'UNCANNY X-MEN (1963) #479',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/787',
              name: 'UNCANNY X-MEN (1963) #481',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/789',
              name: 'UNCANNY X-MEN (1963) #482',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1745',
              name: 'UNCANNY X-MEN (1963) #433',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1776',
              name: 'UNCANNY X-MEN (1963) #434',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/2047',
              name: 'UNCANNY X-MEN (1963) #440',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/2293',
              name: 'UNCANNY X-MEN (1963) #432',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        events: {
          available: 10,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009499/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/233',
              name: 'Atlantis Attacks',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/249',
              name: 'Fatal Attractions',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/251',
              name: 'House of M',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/32',
              name: 'Kings of Pain',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/154',
              name: 'Onslaught',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/336',
              name: 'Secret Empire',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/323',
              name: 'Secret Wars (2015)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/276',
              name: 'War of Kings',
            },
          ],
          returned: 10,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/characters/1771/polaris?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Polaris?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009499/polaris?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
    ],
  },
};

export const mockedStoryComics = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: 'c60ce6e1460c7e1ec9cff559a4edde322a4c846a',
  data: {
    offset: 0,
    limit: 20,
    total: 1,
    count: 1,
    results: [
      {
        id: 12185,
        digitalId: 33244,
        title: 'X-Factor (1986) #109',
        issueNumber: 109,
        variantDescription: '',
        description:
          'In Israel, Mystique lines up her sights on a new target. Only X-Factor can stop her now.',
        modified: '2017-05-11T11:41:11-0400',
        isbn: '',
        upc: '',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 36,
        textObjects: [
          {
            type: 'issue_solicit_text',
            language: 'en-us',
            text: 'In Israel, Mystique lines up her sights on a new target. Only X-Factor can stop her now.',
          },
        ],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/12185',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/12185/x-factor_1986_109?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/X-Factor-109/digital-comic/33244?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'reader',
            url: 'http://marvel.com/digitalcomics/view.htm?iid=33244&utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'inAppLink',
            url: 'https://applink.marvel.com/issue/33244?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/2098',
          name: 'X-Factor (1986 - 1998)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '1994-12-01T00:00:00-0500',
          },
          {
            type: 'focDate',
            date: '-0001-11-30T00:00:00-0500',
          },
          {
            type: 'unlimitedDate',
            date: '2017-05-15T00:00:00-0400',
          },
          {
            type: 'digitalPurchaseDate',
            date: '2014-02-25T00:00:00-0500',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 0,
          },
          {
            type: 'digitalPurchasePrice',
            price: 1.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/d/b0/586516b732193',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/d/b0/586516b732193',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 5,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/12185/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/682',
              name: 'Todd Dezago',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2302',
              name: 'Jan Duursema',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/435',
              name: 'Al Milgrom',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1872',
              name: 'Glynis Oliver',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/350',
              name: 'Richard Starkings',
              role: 'letterer',
            },
          ],
          returned: 5,
        },
        characters: {
          available: 8,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/12185/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009164',
              name: 'Avalanche',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009271',
              name: 'Destiny',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009309',
              name: 'Forge',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009337',
              name: 'Havok',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009399',
              name: 'Legion',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009465',
              name: 'Mystique',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009499',
              name: 'Polaris',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009726',
              name: 'X-Men',
            },
          ],
          returned: 8,
        },
        stories: {
          available: 3,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/12185/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22107',
              name: 'Legion Quest',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22108',
              name: 'The Waking',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/178821',
              name: 'story from X-Factor (1986) #109',
              type: 'interiorStory',
            },
          ],
          returned: 3,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/12185/events',
          items: [],
          returned: 0,
        },
      },
    ],
  },
};
