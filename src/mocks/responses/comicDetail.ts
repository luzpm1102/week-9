export const mockedComicDetail = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: 'da22683b9808bd87bb7cf6571c7cd935ae368d22',
  data: {
    offset: 0,
    limit: 20,
    total: 1,
    count: 1,
    results: [
      {
        id: 1332,
        digitalId: 0,
        title: 'X-Men: Days of Future Past (Trade Paperback)',
        issueNumber: 0,
        variantDescription: '',
        description: '',
        modified: '2017-02-28T14:52:22-0500',
        isbn: '0-7851-1560-9',
        upc: '5960611560-00111',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Trade Paperback',
        pageCount: 144,
        textObjects: [
          {
            type: 'issue_solicit_text',
            language: 'en-us',
            text: '"Re-live the legendary first journey into the dystopian future of 2013 - where Sentinels stalk the Earth, and the X-Men are humanity\'s only hope...until they die! Also featuring the first appearance of Alpha Flight, the return of the Wendigo, the history of the X-Men from Cyclops himself...and a demon for Christmas!? "',
          },
        ],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/1332',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/collection/1332/x-men_days_of_future_past_trade_paperback?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/1327',
          name: 'X-Men: Days of Future Past (2004)',
        },
        variants: [],
        collections: [],
        collectedIssues: [
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/comics/13683',
            name: 'Uncanny X-Men (1963) #142',
          },
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/comics/12460',
            name: 'Uncanny X-Men (1963) #141',
          },
        ],
        dates: [
          {
            type: 'onsaleDate',
            date: '2029-12-31T00:00:00-0500',
          },
          {
            type: 'focDate',
            date: '-0001-11-30T00:00:00-0500',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 9.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/d0/58b5cfb6d5239',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/d0/58b5cfb6d5239',
            extension: 'jpg',
          },
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/b0/4bc66463ef7f0',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/1332/creators',
          items: [],
          returned: 0,
        },
        characters: {
          available: 10,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/1332/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009159',
              name: 'Archangel',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009164',
              name: 'Avalanche',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009199',
              name: 'Blob',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009243',
              name: 'Colossus',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009271',
              name: 'Destiny',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009472',
              name: 'Nightcrawler',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009522',
              name: 'Pyro',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009718',
              name: 'Wolverine',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009726',
              name: 'X-Men',
            },
          ],
          returned: 10,
        },
        stories: {
          available: 3,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/1332/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/15472',
              name: 'Days of Future Past',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27788',
              name: 'Mind Out of Time!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/65738',
              name: 'X-MEN: DAYS OF FUTURE PAST TPB 0 cover',
              type: 'cover',
            },
          ],
          returned: 3,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/1332/events',
          items: [],
          returned: 0,
        },
      },
    ],
  },
};

export const mockedComicCharacters = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: 'b63b06fe0964a85b053b9b522a5ae4383dee50b5',
  data: {
    offset: 0,
    limit: 5,
    total: 10,
    count: 5,
    results: [
      {
        id: 1009159,
        name: 'Archangel',
        description: '',
        modified: '2013-10-18T12:48:24-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/8/03/526165ed93180',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009159',
        comics: {
          available: 561,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009159/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17701',
              name: 'Age of Apocalypse: The Chosen (1995) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12555',
              name: 'All-Winners Comics (1941) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12651',
              name: 'Alpha Flight (1983) #111',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/51178',
              name: 'Archangel (1996) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/63833',
              name: 'Astonishing X-Men (2017) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/64883',
              name: 'Astonishing X-Men (2017) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/65263',
              name: 'Astonishing X-Men (2017) #6',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66265',
              name: 'Astonishing X-Men (2017) #8',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66481',
              name: 'Astonishing X-Men (2017) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67715',
              name: 'Astonishing X-Men (2017) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/40803',
              name: 'Astonishing X-Men (2004) #51',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/71373',
              name: 'Astonishing X-Men Annual (2018) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66299',
              name: 'Astonishing X-Men by Charles Soule Vol. 1: Life of X (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17509',
              name: 'Avengers (1998) #27',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17546',
              name: 'Avengers (1998) #60',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4461',
              name: 'Avengers Assemble Vol. 3 (Hardcover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1098',
              name: 'Avengers Vol. 1: World Trust (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/23971',
              name: 'Cable (2008) #13',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/23972',
              name: 'Cable (2008) #13 (MW, 50/50 Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24171',
              name: 'Cable (2008) #14',
            },
          ],
          returned: 20,
        },
        series: {
          available: 139,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009159/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3614',
              name: 'Age of Apocalypse: The Chosen (1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2114',
              name: 'All-Winners Comics (1941 - 1947)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
              name: 'Alpha Flight (1983 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/19239',
              name: 'Archangel (1996)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/23262',
              name: 'Astonishing X-Men (2017 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/744',
              name: 'Astonishing X-Men (2004 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/26006',
              name: 'Astonishing X-Men Annual (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24018',
              name: 'Astonishing X-Men by Charles Soule Vol. 1: Life of X (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/354',
              name: 'Avengers (1998 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1737',
              name: 'Avengers Assemble Vol. 3 (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/158',
              name: 'Avengers Vol. 1: World Trust (2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3626',
              name: 'Bishop (1994 - 1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1995',
              name: 'Cable (1993 - 2002)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/4002',
              name: 'Cable (2008 - 2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/8',
              name: 'Call, the Vol. 2: The Precinct (2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/485',
              name: 'Captain America (2002 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/690',
              name: 'Captain Marvel (2000 - 2002)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/175',
              name: 'Captain Marvel Vol. 1: Nothing to Lose (2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/105',
              name: 'Captain Marvel Vol. I (1999)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2001',
              name: 'Champions (1975 - 1978)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 622,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009159/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1745',
              name: 'UNCANNY X-MEN (1963) #433',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1776',
              name: 'UNCANNY X-MEN (1963) #434',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/2071',
              name: 'UNCANNY X-MEN (1963) #438',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/2293',
              name: 'UNCANNY X-MEN (1963) #432',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3261',
              name: '2 of 2 - Save the Life of My Child',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/6222',
              name: 'Cover #6222',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/8823',
              name: '[The Six Big Men]',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9053',
              name: 'Cover #9053',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9059',
              name: 'The Case of the Mad Gargoyle',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9719',
              name: 'The House of Horror',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9724',
              name: "Killer's Last Stand",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9729',
              name: 'The Case of the Beggar Prince',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9735',
              name: 'Charity Bazaar Murders',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9741',
              name: "The Devil's Imposter",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9747',
              name: 'Tell-Tale Cigarette',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9753',
              name: 'The Parrot Murder Secret',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9758',
              name: 'Mystery of Horror House',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9763',
              name: 'Adventure of the Generous Fence',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9770',
              name: 'Shadow of the Noose',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/9776',
              name: 'The Two-Faced Corpse',
              type: 'interiorStory',
            },
          ],
          returned: 20,
        },
        events: {
          available: 20,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009159/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/233',
              name: 'Atlantis Attacks',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/240',
              name: 'Days of Future Present',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/246',
              name: 'Evolutionary War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/249',
              name: 'Fatal Attractions',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/302',
              name: 'Fear Itself',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/252',
              name: 'Inferno',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/334',
              name: 'Inhumans Vs. X-Men',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/32',
              name: 'Kings of Pain',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/299',
              name: 'Messiah CompleX',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/298',
              name: 'Messiah War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/263',
              name: 'Mutant Massacre',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/154',
              name: 'Onslaught',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/276',
              name: 'War of Kings',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/277',
              name: 'World War Hulk',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/308',
              name: 'X-Men: Regenesis',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/280',
              name: 'X-Tinction Agenda',
            },
          ],
          returned: 20,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1009159/archangel?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Angel_(Warren_Worthington_III)?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009159/archangel?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1009164,
        name: 'Avalanche',
        description: '',
        modified: '2010-11-05T14:30:34-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/10/4c0042010d383',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009164',
        comics: {
          available: 41,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009164/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/37996',
              name: 'Age of X: Alpha (2010) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/6929',
              name: 'Avengers Annual (1967) #15',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/7467',
              name: 'Cable (1993) #87',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/78389',
              name: 'Excalibur (2019) #8',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/91508',
              name: 'Excalibur (2019) #16',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/126',
              name: 'New Mutants (2003) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/5799',
              name: 'New Mutants Classic Vol. 2 (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/53685',
              name: 'Quicksilver (1997) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43339',
              name: 'Uncanny Avengers (2012) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13683',
              name: 'Uncanny X-Men (1963) #142',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13718',
              name: 'Uncanny X-Men (1963) #177',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13719',
              name: 'Uncanny X-Men (1963) #178',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13740',
              name: 'Uncanny X-Men (1963) #199',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13741',
              name: 'Uncanny X-Men (1963) #200',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13747',
              name: 'Uncanny X-Men (1963) #206',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13766',
              name: 'Uncanny X-Men (1963) #225',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13767',
              name: 'Uncanny X-Men (1963) #226',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13796',
              name: 'Uncanny X-Men (1963) #255',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13942',
              name: 'Uncanny X-Men (1963) #401',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13943',
              name: 'Uncanny X-Men (1963) #402',
            },
          ],
          returned: 20,
        },
        series: {
          available: 18,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009164/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13603',
              name: 'Age of X: Alpha (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1988',
              name: 'Avengers Annual (1967 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1995',
              name: 'Cable (1993 - 2002)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/27547',
              name: 'Excalibur (2019 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/563',
              name: 'New Mutants (2003 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1764',
              name: 'New Mutants Classic Vol. 2 (2007)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/20086',
              name: 'Quicksilver (1997 - 1998)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/16414',
              name: 'Uncanny Avengers (2012 - 2014)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2258',
              name: 'Uncanny X-Men (1963 - 2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/543',
              name: 'Weapon X (2002 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3648',
              name: 'What If? (1989 - 1998)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2098',
              name: 'X-Factor (1986 - 1998)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/6689',
              name: 'X-Factor Annual (1986 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/403',
              name: 'X-Men (2004 - 2007)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2100',
              name: 'X-Men Annual (1970 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1301',
              name: 'X-Men: Day of the Atom (2005)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1327',
              name: 'X-Men: Days of Future Past (2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1318',
              name: "X-MEN: DREAM'S END TPB (2004)",
            },
          ],
          returned: 18,
        },
        stories: {
          available: 48,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009164/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/624',
              name: 'X-MEN (2004) #162',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1888',
              name: 'New Mutants (2003) #9',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1889',
              name: 'Interior #1889',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/15472',
              name: 'Days of Future Past',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/17387',
              name: 'Betrayal',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/19866',
              name: "The Razor's Edge Part 2: The Killing Stroke",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22086',
              name: 'Fallen Angel!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22092',
              name: 'Ambushed!',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22106',
              name: 'Promised Vengeance',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22108',
              name: 'The Waking',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22352',
              name: 'Lost and Found!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22376',
              name: 'Spots!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/24370',
              name: "Dream's End Part II: Life Decisions",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/26136',
              name: 'Heroes and Villains Part One',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/26137',
              name: 'Cover #26137',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/26138',
              name: 'Heroes and Villains Part Two: Treachery',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/26140',
              name: 'Heroes and Villains Part Three: Foreshadowing',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/26142',
              name: 'Heroes and Villains Part Four: Full Circle',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27788',
              name: 'Mind Out of Time!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27864',
              name: 'Sanction',
              type: 'interiorStory',
            },
          ],
          returned: 20,
        },
        events: {
          available: 7,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009164/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/318',
              name: 'Dark Reign',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/246',
              name: 'Evolutionary War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/32',
              name: 'Kings of Pain',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/263',
              name: 'Mutant Massacre',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/270',
              name: 'Secret Wars',
            },
          ],
          returned: 7,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1009164/avalanche?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Avalanche?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009164/avalanche?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1009199,
        name: 'Blob',
        description: '',
        modified: '2011-02-09T17:31:11-0500',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/1/10/4c7c648178328',
          extension: 'png',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009199',
        comics: {
          available: 76,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009199/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38524',
              name: 'Age of X: Universe (2011) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/6929',
              name: 'Avengers Annual (1967) #15',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/7467',
              name: 'Cable (1993) #87',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/66300',
              name: 'Cable & X-Force: Onslaught Rising (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/8261',
              name: 'Daredevil (1964) #269',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4275',
              name: 'Decimation: Generation M (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/16254',
              name: 'Exiles (2001) #14',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1094',
              name: 'Exiles Vol. 3: Out of Time (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/3417',
              name: 'Generation M (2005) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/3463',
              name: 'House of M: Uncanny X-Men (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/9180',
              name: 'Incredible Hulk (1962) #369',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1587',
              name: 'MARVEL MASTERWORKS: THE UNCANNY X-MEN VOL. 2 HC (Hardcover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/14796',
              name: 'Peter Parker, the Spectacular Spider-Man (1976) #91',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43455',
              name: 'Amazing Spider-Man (1999) #11',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/36153',
              name: 'Uncanny X-Force (2010) #16',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/40447',
              name: 'Uncanny X-Force (2010) #27',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/40442',
              name: 'Uncanny X-Force (2010) #30',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12539',
              name: 'Uncanny X-Men (1963) #86',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12459',
              name: 'Uncanny X-Men (1963) #140',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13683',
              name: 'Uncanny X-Men (1963) #142',
            },
          ],
          returned: 20,
        },
        series: {
          available: 31,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009199/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13896',
              name: 'Age of X: Universe (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/454',
              name: 'Amazing Spider-Man (1999 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1988',
              name: 'Avengers Annual (1967 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1995',
              name: 'Cable (1993 - 2002)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24019',
              name: 'Cable & X-Force: Onslaught Rising (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2002',
              name: 'Daredevil (1964 - 1998)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1635',
              name: 'Decimation: Generation M (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/479',
              name: 'Exiles (2001 - 2008)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/154',
              name: 'Exiles Vol. 3: Out of Time (2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/973',
              name: 'Generation M (2005 - 2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1412',
              name: 'House of M: Uncanny X-Men (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2021',
              name: 'Incredible Hulk (1962 - 1999)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1440',
              name: 'MARVEL MASTERWORKS: THE UNCANNY X-MEN VOL. 2 HC (2005)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2271',
              name: 'Peter Parker, the Spectacular Spider-Man (1976 - 1998)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9976',
              name: 'Uncanny X-Force (2010 - 2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2258',
              name: 'Uncanny X-Men (1963 - 2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/543',
              name: 'Weapon X (2002 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3648',
              name: 'What If? (1989 - 1998)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/6688',
              name: 'X-51 (1999 - 2000)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2098',
              name: 'X-Factor (1986 - 1998)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 79,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009199/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/752',
              name: 'Uncanny X-Men (1963) #463',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/753',
              name: '2 of 4 - Season of the Witch',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/755',
              name: '4 of 4 - Season of the Witch (HoM)',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/5486',
              name: '3 of 5 - 5XLS',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/5487',
              name: '3 of 5 - 5XLS',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/15470',
              name: 'Rage!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/15472',
              name: 'Days of Future Past',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/15507',
              name: 'Uncanny X-Men (1963) #3',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/15624',
              name: 'Uncanny X-Men (1963) #7',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/15663',
              name: 'Cover #15663',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/16097',
              name: 'DAREDEVIL (1964) #269',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/17387',
              name: 'Betrayal',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/18887',
              name: 'Incredible Hulk (1962) #369',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/18888',
              name: 'Silent Screams',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22086',
              name: 'Fallen Angel!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22103',
              name: 'Guido vs. Blob',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22237',
              name: 'Kiss Of Death!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22243',
              name: 'For All The World To See',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22259',
              name: 'Dust To Dust',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22352',
              name: 'Lost and Found!',
              type: 'interiorStory',
            },
          ],
          returned: 20,
        },
        events: {
          available: 7,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009199/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/246',
              name: 'Evolutionary War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/251',
              name: 'House of M',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/263',
              name: 'Mutant Massacre',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/270',
              name: 'Secret Wars',
            },
          ],
          returned: 7,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/characters/308/blob?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Blob?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009199/blob?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1009243,
        name: 'Colossus',
        description: '',
        modified: '2016-02-11T10:06:50-0500',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/e0/51127cf4b996f',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009243',
        comics: {
          available: 696,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009243/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17701',
              name: 'Age of Apocalypse: The Chosen (1995) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/37996',
              name: 'Age of X: Alpha (2010) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38524',
              name: 'Age of X: Universe (2011) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38523',
              name: 'Age of X: Universe (2011) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12676',
              name: 'Alpha Flight (1983) #17',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12694',
              name: 'Alpha Flight (1983) #33',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12725',
              name: 'Alpha Flight (1983) #61',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/51348',
              name: 'Amazing X-Men (2013) #19',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1808',
              name: 'Astonishing X-Men (2004) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/843',
              name: 'Astonishing X-Men (2004) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/927',
              name: 'Astonishing X-Men (2004) #6',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1436',
              name: 'Astonishing X-Men (2004) #8',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/2842',
              name: 'Astonishing X-Men (2004) #11',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/2161',
              name: 'Astonishing X-Men (2004) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/3475',
              name: 'Astonishing X-Men (2004) #13',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67717',
              name: 'Astonishing X-Men (2017) #14',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4022',
              name: 'Astonishing X-Men (2004) #14',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67718',
              name: 'Astonishing X-Men (2017) #15',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4253',
              name: 'Astonishing X-Men (2004) #15',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4736',
              name: 'Astonishing X-Men (2004) #16',
            },
          ],
          returned: 20,
        },
        series: {
          available: 167,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009243/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3614',
              name: 'Age of Apocalypse: The Chosen (1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13603',
              name: 'Age of X: Alpha (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13896',
              name: 'Age of X: Universe (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
              name: 'Alpha Flight (1983 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/18142',
              name: 'Amazing X-Men (2013 - 2015)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/744',
              name: 'Astonishing X-Men (2004 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/23262',
              name: 'Astonishing X-Men (2017 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/7576',
              name: 'Astonishing X-Men by Joss Whedon & John Cassaday (2009 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1464',
              name: 'Astonishing X-Men Vol. 1 (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1298',
              name: 'Astonishing X-Men Vol. 1: Gifted (2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1422',
              name: 'Astonishing X-Men Vol. 2: Dangerous (2005)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1485',
              name: 'Astonishing X-Men Vol. 3: Torn (2007)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/5055',
              name: 'Astonishing X-Men Vol. 4: Unstoppable (2008)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1991',
              name: 'Avengers (1963 - 1996)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/354',
              name: 'Avengers (1998 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1340',
              name: 'Avengers Assemble (2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1816',
              name: 'Avengers Assemble Vol. 4 (2007)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/15305',
              name: 'Avengers Vs. X-Men (2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/784',
              name: 'Black Panther (2005 - 2008)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/4002',
              name: 'Cable (2008 - 2010)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 853,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009243/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/477',
              name: 'Cover #477',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/743',
              name: "5 of 5 - World's End",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/745',
              name: '1 of 2 - Mojo Rising',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/747',
              name: '2 of 2 - Mojo Rising',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/763',
              name: "3 of 3 - Grey's End",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1454',
              name: 'Ultimate X-Men (2001) #61',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/2843',
              name: '1 of 1 - TBD',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3321',
              name: 'Astonishing X-Men (2004) #5',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3322',
              name: '“GIFTED” 5 (OF 6)  As demand for the',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3323',
              name: 'Cover for Astonishing X-Men (2004) #6',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3324',
              name: '“GIFTED” PART 6 (OF 6) Outnumbered and outgunned, the X-Men are finally brought together as a team by their newest addition – bu',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3326',
              name: '2 of 6 - Dangerous',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3332',
              name: 'Interior #3332',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3335',
              name: '6 of 6 - Dangerous',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3336',
              name: '6 of 6 - Dangerous',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3337',
              name: 'Interior #3337',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3338',
              name: '1 of 6',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3339',
              name: '1 of 6',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3341',
              name: '2 of 6 - Torn',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3343',
              name: '3 of 6 - Torn',
              type: 'interiorStory',
            },
          ],
          returned: 20,
        },
        events: {
          available: 27,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009243/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/233',
              name: 'Atlantis Attacks',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/310',
              name: 'Avengers VS X-Men',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/238',
              name: 'Civil War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/318',
              name: 'Dark Reign',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/240',
              name: 'Days of Future Present',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/246',
              name: 'Evolutionary War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/249',
              name: 'Fatal Attractions',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/302',
              name: 'Fear Itself',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/252',
              name: 'Inferno',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/32',
              name: 'Kings of Pain',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/37',
              name: 'Maximum Security',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/299',
              name: 'Messiah CompleX',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/263',
              name: 'Mutant Massacre',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/154',
              name: 'Onslaught',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/336',
              name: 'Secret Empire',
            },
          ],
          returned: 20,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1009243/colossus?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Colossus_(Piotr_Rasputin)?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009243/colossus?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1009271,
        name: 'Destiny',
        description: '',
        modified: '1969-12-31T19:00:00-0500',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009271',
        comics: {
          available: 43,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009271/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/6929',
              name: 'Avengers Annual (1967) #15',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/96222',
              name: 'Inferno (2021) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/96223',
              name: 'Inferno (2021) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/96224',
              name: 'Inferno (2021) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/96225',
              name: 'Inferno (2021) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/19816',
              name: 'Marvel Fanfare (1982) #40',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1500',
              name: 'MARVEL MASTERWORKS: THE INCREDIBLE HULK VOL. 2 HC (Hardcover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15912',
              name: 'Marvel Masterworks: The Sub-Mariner Vol. 2 (Hardcover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/95052',
              name: "Marvel's Voices: Pride (2021) #1",
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/34025',
              name: 'Moon Knight (2011) #7',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17062',
              name: 'Sub-Mariner (1968) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/11350',
              name: 'Tales to Astonish (1959) #101',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13682',
              name: 'Uncanny X-Men (1963)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13683',
              name: 'Uncanny X-Men (1963) #142',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13711',
              name: 'Uncanny X-Men (1963) #170',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13718',
              name: 'Uncanny X-Men (1963) #177',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13719',
              name: 'Uncanny X-Men (1963) #178',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13726',
              name: 'Uncanny X-Men (1963) #185',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13740',
              name: 'Uncanny X-Men (1963) #199',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13741',
              name: 'Uncanny X-Men (1963) #200',
            },
          ],
          returned: 20,
        },
        series: {
          available: 20,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009271/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1988',
              name: 'Avengers Annual (1967 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/32954',
              name: 'Inferno (2021 - 2022)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3719',
              name: 'Marvel Fanfare (1982 - 1992)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1404',
              name: 'MARVEL MASTERWORKS: THE INCREDIBLE HULK VOL. 2 HC (2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2441',
              name: 'Marvel Masterworks: The Sub-Mariner Vol. 2 (2007)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/32375',
              name: "Marvel's Voices: Pride (2021)",
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/10639',
              name: 'Moon Knight (2011 - 2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2989',
              name: 'Sub-Mariner (1968 - 1974)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2080',
              name: 'Tales to Astonish (1959 - 1968)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2258',
              name: 'Uncanny X-Men (1963 - 2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1777',
              name: 'Women of Marvel (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2098',
              name: 'X-Factor (1986 - 1998)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/25605',
              name: 'X-FORCE -1 (1997 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2265',
              name: 'X-Men (1991 - 2001)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1327',
              name: 'X-Men: Days of Future Past (2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2101',
              name: 'X-Men: Omega (1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1685',
              name: 'X-Men: The Complete Age of Apocalypse Epic Book 4 (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/646',
              name: 'X-Treme X-Men (2001 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2107',
              name: 'X-Treme X-Men: Savage Land (2001 - 2002)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/144',
              name: 'X-Treme X-Men: Savage Land (1999)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 43,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009271/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/12013',
              name: '...And Evil Shall Beckon!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/15472',
              name: 'Days of Future Past',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/17387',
              name: 'Betrayal',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22086',
              name: 'Fallen Angel!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22108',
              name: 'The Waking',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22122',
              name: 'Cover #22122',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22123',
              name: 'Reaching out to Yesterday',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22237',
              name: 'Kiss Of Death!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22352',
              name: 'Lost and Found!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/22376',
              name: 'Spots!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/24492',
              name: 'Interior #24492',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/24493',
              name: 'Endings',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/26044',
              name: 'Savage Genesis',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27785',
              name: 'The Boy Who Saw Tomorrow!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27788',
              name: 'Mind Out of Time!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27850',
              name: "Dancin' In the Dark",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27864',
              name: 'Sanction',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27866',
              name: 'Hell Hath No Fury',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27880',
              name: 'Public Enemy',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27908',
              name: 'The Spiral Path',
              type: 'interiorStory',
            },
          ],
          returned: 20,
        },
        events: {
          available: 4,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009271/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/263',
              name: 'Mutant Massacre',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/270',
              name: 'Secret Wars',
            },
          ],
          returned: 4,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/characters/543/destiny?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Destiny_(Irene_Adler)?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009271/destiny?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
    ],
  },
};

export const mockedComicStories = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '71597ee68f4a828f67c127184aed6e5f241c8505',
  data: {
    offset: 0,
    limit: 5,
    total: 3,
    count: 3,
    results: [
      {
        id: 15472,
        title: 'Days of Future Past',
        description:
          'Rachel Summers sends Kitty Pryde back in time from 2013 to warn the X-Men that the new Brotherhood of Evil Mutants will try to kill Senator Kelly and begin a chain of events that spells doom for mutants.',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/15472',
        type: 'story',
        modified: '1969-12-31T19:00:00-0500',
        thumbnail: null,
        creators: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/15472/creators',
          items: [],
          returned: 0,
        },
        characters: {
          available: 10,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/15472/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009159',
              name: 'Archangel',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009164',
              name: 'Avalanche',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009199',
              name: 'Blob',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009243',
              name: 'Colossus',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009271',
              name: 'Destiny',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009472',
              name: 'Nightcrawler',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009522',
              name: 'Pyro',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009718',
              name: 'Wolverine',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009726',
              name: 'X-Men',
            },
          ],
          returned: 10,
        },
        series: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/15472/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2258',
              name: 'Uncanny X-Men (1963 - 2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1327',
              name: 'X-Men: Days of Future Past (2004)',
            },
          ],
          returned: 2,
        },
        comics: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/15472/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12460',
              name: 'Uncanny X-Men (1963) #141',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1332',
              name: 'X-Men: Days of Future Past (Trade Paperback)',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/15472/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/12460',
          name: 'Uncanny X-Men (1963) #141',
        },
      },
      {
        id: 27788,
        title: 'Mind Out of Time!',
        description:
          "The Brotherhood make its play against Senator Kelly; Rachel Summers (in Kitty Pryde's body) helps the X-Men defeat Mystique and her crew (thus changing the future); Kitty returns from 2013.",
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/27788',
        type: 'story',
        modified: '1969-12-31T19:00:00-0500',
        thumbnail: null,
        creators: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/27788/creators',
          items: [],
          returned: 0,
        },
        characters: {
          available: 10,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/27788/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009159',
              name: 'Archangel',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009164',
              name: 'Avalanche',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009199',
              name: 'Blob',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009243',
              name: 'Colossus',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009271',
              name: 'Destiny',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009472',
              name: 'Nightcrawler',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009522',
              name: 'Pyro',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009718',
              name: 'Wolverine',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009726',
              name: 'X-Men',
            },
          ],
          returned: 10,
        },
        series: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/27788/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2258',
              name: 'Uncanny X-Men (1963 - 2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1327',
              name: 'X-Men: Days of Future Past (2004)',
            },
          ],
          returned: 2,
        },
        comics: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/27788/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13683',
              name: 'Uncanny X-Men (1963) #142',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1332',
              name: 'X-Men: Days of Future Past (Trade Paperback)',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/27788/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/13683',
          name: 'Uncanny X-Men (1963) #142',
        },
      },
      {
        id: 65738,
        title: 'X-MEN: DAYS OF FUTURE PAST TPB 0 cover',
        description: '',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/65738',
        type: 'cover',
        modified: '2017-02-28T14:30:28-0500',
        thumbnail: null,
        creators: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/65738/creators',
          items: [],
          returned: 0,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/65738/characters',
          items: [],
          returned: 0,
        },
        series: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/65738/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1327',
              name: 'X-Men: Days of Future Past (2004)',
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/65738/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1332',
              name: 'X-Men: Days of Future Past (Trade Paperback)',
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/65738/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/1332',
          name: 'X-Men: Days of Future Past (Trade Paperback)',
        },
      },
    ],
  },
};
