export const mockedCharacterDetail = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: 'ea7555d01a443e1144f45c29413d9ffcb9bd7ee6',
  data: {
    offset: 0,
    limit: 20,
    total: 1,
    count: 1,
    results: [
      {
        id: 1009629,
        name: 'Storm',
        description:
          'Ororo Monroe is the descendant of an ancient line of African priestesses, all of whom have white hair, blue eyes, and the potential to wield magic.',
        modified: '2016-05-26T11:50:27-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/40/526963dad214d',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009629',
        comics: {
          available: 852,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009629/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17701',
              name: 'Age of Apocalypse: The Chosen (1995) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43498',
              name: 'A+X (2012) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/37996',
              name: 'Age of X: Alpha (2010) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12676',
              name: 'Alpha Flight (1983) #17',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12694',
              name: 'Alpha Flight (1983) #33',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12725',
              name: 'Alpha Flight (1983) #61',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12668',
              name: 'Alpha Flight (1983) #127',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/21511',
              name: 'Astonishing X-Men (2004) #25',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/21714',
              name: 'Astonishing X-Men (2004) #26',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/21941',
              name: 'Astonishing X-Men (2004) #27',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/23087',
              name: 'Astonishing X-Men (2004) #28',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/23937',
              name: 'Astonishing X-Men (2004) #29',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24501',
              name: 'Astonishing X-Men (2004) #30',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24503',
              name: 'Astonishing X-Men (2004) #32',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24504',
              name: 'Astonishing X-Men (2004) #33',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24505',
              name: 'Astonishing X-Men (2004) #34',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/30332',
              name: 'Astonishing X-Men (2004) #35',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38318',
              name: 'Astonishing X-Men (2004) #38',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38319',
              name: 'Astonishing X-Men (2004) #40',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/39318',
              name: 'Astonishing X-Men (2004) #44',
            },
          ],
          returned: 20,
        },
        series: {
          available: 220,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009629/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/16450',
              name: 'A+X (2012 - 2014)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3614',
              name: 'Age of Apocalypse: The Chosen (1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13603',
              name: 'Age of X: Alpha (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
              name: 'Alpha Flight (1983 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/744',
              name: 'Astonishing X-Men (2004 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/31906',
              name: 'Atlantis Attacks: The Original Epic (2021)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1991',
              name: 'Avengers (1963 - 1996)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/354',
              name: 'Avengers (1998 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9085',
              name: 'Avengers (2010 - 2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1340',
              name: 'Avengers Assemble (2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/15305',
              name: 'Avengers Vs. X-Men (2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3626',
              name: 'Bishop (1994 - 1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/784',
              name: 'Black Panther (2005 - 2008)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/31553',
              name: 'Black Panther (2021 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/6804',
              name: 'Black Panther (2009 - 2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/20912',
              name: 'Black Panther (2016 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2115',
              name: 'Black Panther (1998 - 2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24291',
              name: 'Black Panther (2018 - 2021)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/23017',
              name: 'Black Panther and the Crew (2017)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3850',
              name: 'Black Panther Annual (2008)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 991,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009629/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/497',
              name: 'Interior #497',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/648',
              name: '1 of 2- Black Panther crossover',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/650',
              name: '2 of 2- Black Panther crossover',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/737',
              name: '2 of 5 - Savage Land',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/738',
              name: 'Uncanny X-Men (1963) #457',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/739',
              name: '3 of 5 - Savage Land',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/742',
              name: 'Uncanny X-Men (1963) #459',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/743',
              name: "5 of 5 - World's End",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/745',
              name: '1 of 2 - Mojo Rising',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/747',
              name: '2 of 2 - Mojo Rising',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/749',
              name: '1 of 4 - Season of the Witch',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/760',
              name: 'Uncanny X-Men (1963) #467',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/761',
              name: "2 of 3 - Grey's End",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/765',
              name: '1 of 3 -',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/767',
              name: "2 of 3 - Wand'ring Star",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/768',
              name: 'UNCANNY X-MEN (1963) #471',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/770',
              name: 'UNCANNY X-MEN (1963) #472',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1420',
              name: 'ULTIMATE X-MEN (2000) #43',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1422',
              name: 'ULTIMATE X-MEN (2000) #44',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1426',
              name: 'ULTIMATE X-MEN (2000) #49',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        events: {
          available: 30,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009629/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/233',
              name: 'Atlantis Attacks',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/310',
              name: 'Avengers VS X-Men',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/238',
              name: 'Civil War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/318',
              name: 'Dark Reign',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/240',
              name: 'Days of Future Present',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/245',
              name: 'Enemy of the State',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/246',
              name: 'Evolutionary War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/249',
              name: 'Fatal Attractions',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/251',
              name: 'House of M',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/252',
              name: 'Inferno',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/334',
              name: 'Inhumans Vs. X-Men',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/255',
              name: 'Initiative',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/37',
              name: 'Maximum Security',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/299',
              name: 'Messiah CompleX',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/263',
              name: 'Mutant Massacre',
            },
          ],
          returned: 20,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1009629/storm?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Storm?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009629/storm?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
    ],
  },
};

export const mockedCharacterComics = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: 'b4786e26b7b29d193c3eff2547a24fe8209d5567',
  data: {
    offset: 0,
    limit: 5,
    total: 852,
    count: 5,
    results: [
      {
        id: 1332,
        digitalId: 0,
        title: 'X-Men: Days of Future Past (Trade Paperback)',
        issueNumber: 0,
        variantDescription: '',
        description: '',
        modified: '2017-02-28T14:52:22-0500',
        isbn: '0-7851-1560-9',
        upc: '5960611560-00111',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Trade Paperback',
        pageCount: 144,
        textObjects: [
          {
            type: 'issue_solicit_text',
            language: 'en-us',
            text: '"Re-live the legendary first journey into the dystopian future of 2013 - where Sentinels stalk the Earth, and the X-Men are humanity\'s only hope...until they die! Also featuring the first appearance of Alpha Flight, the return of the Wendigo, the history of the X-Men from Cyclops himself...and a demon for Christmas!? "',
          },
        ],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/1332',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/collection/1332/x-men_days_of_future_past_trade_paperback?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/1327',
          name: 'X-Men: Days of Future Past (2004)',
        },
        variants: [],
        collections: [],
        collectedIssues: [
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/comics/13683',
            name: 'Uncanny X-Men (1963) #142',
          },
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/comics/12460',
            name: 'Uncanny X-Men (1963) #141',
          },
        ],
        dates: [
          {
            type: 'onsaleDate',
            date: '2029-12-31T00:00:00-0500',
          },
          {
            type: 'focDate',
            date: '-0001-11-30T00:00:00-0500',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 9.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/d0/58b5cfb6d5239',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/d0/58b5cfb6d5239',
            extension: 'jpg',
          },
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/b0/4bc66463ef7f0',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/1332/creators',
          items: [],
          returned: 0,
        },
        characters: {
          available: 10,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/1332/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009159',
              name: 'Archangel',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009164',
              name: 'Avalanche',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009199',
              name: 'Blob',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009243',
              name: 'Colossus',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009271',
              name: 'Destiny',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009472',
              name: 'Nightcrawler',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009522',
              name: 'Pyro',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009718',
              name: 'Wolverine',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009726',
              name: 'X-Men',
            },
          ],
          returned: 10,
        },
        stories: {
          available: 3,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/1332/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/15472',
              name: 'Days of Future Past',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/27788',
              name: 'Mind Out of Time!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/65738',
              name: 'X-MEN: DAYS OF FUTURE PAST TPB 0 cover',
              type: 'cover',
            },
          ],
          returned: 3,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/1332/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 3627,
        digitalId: 0,
        title: 'Storm (2006)',
        issueNumber: 0,
        variantDescription: '',
        description: '#N/A',
        modified: '2015-01-29T20:04:55-0500',
        isbn: '',
        upc: '',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 0,
        textObjects: [
          {
            type: 'issue_solicit_text',
            language: 'en-us',
            text: "The epic, untold love story between Marvel's two pre-eminent Black super heroes -- Storm and the Black Panther -- is finally told, as only New York Times best-selling author Eric Jerome Dickey can do it!  An orphaned street urchin, living by her wits on the unforgiving plains of Africa as she struggles to harness her slowly-developing mutant powers...A warrior Prince, embarking on his rite of passage as he ponders the great responsibility in his future...And a crew of ruthless mercenaries who'll stop at nothing to capture an elusive creature of legend -- the fabled wind-rider.  What sparks occur when their paths intersect?  Don't miss out on this story, True Believer, as it builds to a July Event that will shake the entire Marvel Universe.\r<br>\r<br>32 PGS./T+ SUGGESTED FOR TEENS AND UP ...$2.99\r<br>",
          },
        ],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/3627',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/3627/storm_2006?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/357',
          name: 'Storm (2006)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2029-12-31T00:00:00-0500',
          },
          {
            type: 'focDate',
            date: '-0001-11-30T00:00:00-0500',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 0,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/80/4bc5fe7a308d7',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/80/4bc5fe7a308d7',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 3,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/3627/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/370',
              name: 'Eric Jerome Dickey',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/371',
              name: 'David Hine',
              role: 'penciller',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/243',
              name: 'Mike Mayhew',
              role: 'penciller (cover)',
            },
          ],
          returned: 3,
        },
        characters: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/3627/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
          ],
          returned: 1,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/3627/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/496',
              name: 'Cover #496',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/497',
              name: 'Interior #497',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/3627/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 96266,
        digitalId: 0,
        title: 'X-Men Red (2022) #2',
        issueNumber: 2,
        variantDescription: '',
        description: null,
        modified: '2021-12-18T09:20:57-0500',
        isbn: '',
        upc: '75960620212600211',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 32,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/96266',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/96266/x-men_red_2022_2?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/X-Men-Red-2/digital-comic/59186?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/32962',
          name: 'X-Men Red (2022 - Present)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2022-05-18T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2022-04-18T00:00:00-0400',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 3.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/1/20/627534b6977a6',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/1/20/627534b6977a6',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 7,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/96266/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/13180',
              name: 'Federico Blee',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1133',
              name: 'Stefano Caselli',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12449',
              name: 'Russell Dauterman',
              role: 'penciler (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12174',
              name: 'Al Ewing',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12980',
              name: 'Vc Cory Petit',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/14311',
              name: 'Jordan White',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/10279',
              name: 'Matthew Wilson',
              role: 'colorist (cover)',
            },
          ],
          returned: 7,
        },
        characters: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/96266/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1011011',
              name: 'Vulcan (Gabriel Summers)',
            },
          ],
          returned: 2,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/96266/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/213597',
              name: 'cover from X-Men: Red (2022) #2',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/213598',
              name: 'story from X-Men: Red (2022) #2',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/96266/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 80587,
        digitalId: 0,
        title: 'The Marvels (2021) #10',
        issueNumber: 10,
        variantDescription: '',
        description: null,
        modified: '2021-12-08T09:09:59-0500',
        isbn: '',
        upc: '759606095941001011',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 32,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/80587',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/80587/the_marvels_2021_10?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/The-Marvels-10/digital-comic/59172?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/28381',
          name: 'The Marvels (2021 - Present)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2022-05-18T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2022-04-11T00:00:00-0400',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 3.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/627535171b6fa',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/50/627535171b6fa',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 6,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/80587/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4992',
              name: 'Simon Bowland',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2133',
              name: 'Tom Brevoort',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1231',
              name: 'Kurt Busiek',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12448',
              name: 'Yildiray Cinar',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12239',
              name: 'Guru Efx',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/63',
              name: 'Alex Ross',
              role: 'painter (cover)',
            },
          ],
          returned: 6,
        },
        characters: {
          available: 4,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/80587/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1017851',
              name: 'Aero (Aero)',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009165',
              name: 'Avengers',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009185',
              name: 'Black Cat',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
          ],
          returned: 4,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/80587/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/178300',
              name: 'cover from The Marvels (2029) #10',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/178301',
              name: 'story from The Marvels (2029) #10',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/80587/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 96265,
        digitalId: 0,
        title: 'X-Men Red (2022) #1',
        issueNumber: 1,
        variantDescription: '',
        description: null,
        modified: '2022-04-20T09:07:00-0400',
        isbn: '',
        upc: '75960620212600111',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 40,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/96265',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/96265/x-men_red_2022_1?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/X-Men-Red-1/digital-comic/59185?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/32962',
          name: 'X-Men Red (2022 - Present)',
        },
        variants: [
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/comics/99899',
            name: 'X-Men Red (2022) #1 (Variant)',
          },
        ],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2022-05-11T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2022-03-07T00:00:00-0500',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 4.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/70/62432e4f27fcd',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/70/62432e4f27fcd',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 7,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/96265/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/13180',
              name: 'Federico Blee',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1133',
              name: 'Stefano Caselli',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12449',
              name: 'Russell Dauterman',
              role: 'penciler (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12174',
              name: 'Al Ewing',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/14075',
              name: 'Vc Ariana Maher',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/14311',
              name: 'Jordan White',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/10279',
              name: 'Matthew Wilson',
              role: 'colorist (cover)',
            },
          ],
          returned: 7,
        },
        characters: {
          available: 6,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/96265/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1011297',
              name: 'Agent Brand',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009214',
              name: 'Cable',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009417',
              name: 'Magneto',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009638',
              name: 'Sunspot',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1011011',
              name: 'Vulcan (Gabriel Summers)',
            },
          ],
          returned: 6,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/96265/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/213595',
              name: 'cover from X-Men: Red (2022) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/213596',
              name: 'story from X-Men: Red (2022) #1',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/96265/events',
          items: [],
          returned: 0,
        },
      },
    ],
  },
};

export const mockedCharacterStories = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '1802e9c6d0033ce8ce9bcc7531261f233962a4dd',
  data: {
    offset: 0,
    limit: 5,
    total: 981,
    count: 5,
    results: [
      {
        id: 497,
        title: 'Interior #497',
        description: '',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/497',
        type: 'story',
        modified: '1969-12-31T19:00:00-0500',
        thumbnail: null,
        creators: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/497/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/370',
              name: 'Eric Jerome Dickey',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/371',
              name: 'David Hine',
              role: 'penciller',
            },
          ],
          returned: 2,
        },
        characters: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/497/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
          ],
          returned: 1,
        },
        series: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/497/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/357',
              name: 'Storm (2006)',
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/497/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/3627',
              name: 'Storm (2006)',
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/497/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/3627',
          name: 'Storm (2006)',
        },
      },
      {
        id: 648,
        title: '1 of 2- Black Panther crossover',
        description: '',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/648',
        type: 'cover',
        modified: '2017-02-23T15:45:24-0500',
        thumbnail: null,
        creators: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/648/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11757',
              name: 'Salvador Larroca',
              role: 'penciller (cover)',
            },
          ],
          returned: 1,
        },
        characters: {
          available: 6,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/648/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009313',
              name: 'Gambit',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009337',
              name: 'Havok',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009362',
              name: 'Iceman',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009499',
              name: 'Polaris',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009546',
              name: 'Rogue',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
          ],
          returned: 6,
        },
        series: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/648/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/403',
              name: 'X-Men (2004 - 2007)',
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/648/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/2410',
              name: 'X-Men (2004) #175',
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/648/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/2410',
          name: 'X-Men (2004) #175',
        },
      },
      {
        id: 650,
        title: '2 of 2- Black Panther crossover',
        description: '',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/650',
        type: 'cover',
        modified: '2017-02-23T15:47:59-0500',
        thumbnail: null,
        creators: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/650/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11757',
              name: 'Salvador Larroca',
              role: 'penciller (cover)',
            },
          ],
          returned: 1,
        },
        characters: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/650/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009187',
              name: 'Black Panther',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
          ],
          returned: 2,
        },
        series: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/650/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/403',
              name: 'X-Men (2004 - 2007)',
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/650/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/2465',
              name: 'X-Men (2004) #176',
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/650/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/2465',
          name: 'X-Men (2004) #176',
        },
      },
      {
        id: 737,
        title: '2 of 5 - Savage Land',
        description: '',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/737',
        type: 'story',
        modified: '2017-05-22T11:09:03-0400',
        thumbnail: null,
        creators: {
          available: 5,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/737/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/44',
              name: 'Chris Claremont',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/16',
              name: 'Alan Davis',
              role: 'penciller',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/440',
              name: 'Mark Farmer',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4174',
              name: 'Mike Marts',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/442',
              name: 'Dean White',
              role: 'colorist',
            },
          ],
          returned: 5,
        },
        characters: {
          available: 8,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/737/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009182',
              name: 'Bishop',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009472',
              name: 'Nightcrawler',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009512',
              name: 'Psylocke',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009430',
              name: 'Rachel Grey',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009718',
              name: 'Wolverine',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009722',
              name: 'X-23',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009726',
              name: 'X-Men',
            },
          ],
          returned: 8,
        },
        series: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/737/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2258',
              name: 'Uncanny X-Men (1963 - 2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1399',
              name: 'Uncanny X-Men - The New Age Vol. 3: On Ice (2005)',
            },
          ],
          returned: 2,
        },
        comics: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/737/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1664',
              name: 'Uncanny X-Men (1963) #456',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/2299',
              name: 'Uncanny X-Men - The New Age Vol. 3: On Ice (Trade Paperback)',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/737/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/1664',
          name: 'Uncanny X-Men (1963) #456',
        },
      },
      {
        id: 738,
        title: 'Uncanny X-Men (1963) #457',
        description:
          'High adventure in the Savage Land! The X-Men discover a wondrous and advanced new civilization…but it’s none too friendly toward humans—or mutants!',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/738',
        type: 'cover',
        modified: '2017-05-22T11:38:29-0400',
        thumbnail: null,
        creators: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/738/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/16',
              name: 'Alan Davis',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4174',
              name: 'Mike Marts',
              role: 'editor',
            },
          ],
          returned: 2,
        },
        characters: {
          available: 7,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/738/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009182',
              name: 'Bishop',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009472',
              name: 'Nightcrawler',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009512',
              name: 'Psylocke',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009430',
              name: 'Rachel Grey',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009722',
              name: 'X-23',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009726',
              name: 'X-Men',
            },
          ],
          returned: 7,
        },
        series: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/738/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2258',
              name: 'Uncanny X-Men (1963 - 2011)',
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/738/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1758',
              name: 'Uncanny X-Men (1963) #457',
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/738/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/1758',
          name: 'Uncanny X-Men (1963) #457',
        },
      },
    ],
  },
};

export const mockedCharacterSeries = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: 'bad4838775208b4a2e4c89d58700808c0b20830a',
  data: {
    offset: 0,
    limit: 5,
    total: 220,
    count: 5,
    results: [
      {
        id: 16450,
        title: 'A+X (2012 - 2014)',
        description:
          "Get ready for action-packed stories featuring team-ups from your favorite Marvel heroes every month! First, a story where Wolverine and Hulk come together, and then Captain America and Cable meet up! But will each partner's combined strength be enough?",
        resourceURI: 'http://gateway.marvel.com/v1/public/series/16450',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/series/16450/ax_2012_-_2014?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
        ],
        startYear: 2012,
        endYear: 2014,
        rating: '',
        type: '',
        modified: '2019-09-05T14:32:24-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/5/d0/511e88a20ae34',
          extension: 'jpg',
        },
        creators: {
          available: 87,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/16450/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11463',
              name: 'Jason Aaron',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/191',
              name: 'Kaare Andrews',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/8658',
              name: 'James Asmus',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/232',
              name: 'Chris Bachalo',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/9127',
              name: 'Mike Benson',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/808',
              name: 'Howard Chaykin',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12452',
              name: 'Mike Costa',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4014',
              name: 'Axel Alonso',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11575',
              name: 'Kris Anka',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1141',
              name: 'Giuseppe Camuncoli',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1133',
              name: 'Stefano Caselli',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/506',
              name: 'Joe Bennett',
              role: 'penciller',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/13215',
              name: 'Rain Beredo',
              role: 'colorist (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/694',
              name: 'Mark Brooks',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11543',
              name: 'Dan Brown',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12604',
              name: 'Jim Campbell',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/9937',
              name: 'Jim Charalampidis',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/665',
              name: 'Reilly Brown',
              role: 'artist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12467',
              name: 'Orphans Cheeps',
              role: 'artist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/452',
              name: 'Virtual Calligr',
              role: 'letterer',
            },
          ],
          returned: 20,
        },
        characters: {
          available: 37,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/16450/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009148',
              name: 'Absorbing Man',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009175',
              name: 'Beast',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009187',
              name: 'Black Panther',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009189',
              name: 'Black Widow',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009211',
              name: 'Bucky',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009214',
              name: 'Cable',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009220',
              name: 'Captain America',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1010338',
              name: 'Captain Marvel (Carol Danvers)',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009268',
              name: 'Deadpool',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009282',
              name: 'Doctor Strange',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009277',
              name: 'Domino',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009279',
              name: 'Doop',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009301',
              name: 'Fantomex',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009313',
              name: 'Gambit',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009338',
              name: 'Hawkeye',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009351',
              name: 'Hulk',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009362',
              name: 'Iceman',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009367',
              name: 'Iron Fist (Danny Rand)',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009368',
              name: 'Iron Man',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009508',
              name: 'Kitty Pryde',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 66,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/16450/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97110',
              name: 'A+X (2012) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97166',
              name: 'cover from All-New X-Men (2012) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97167',
              name: 'story from All-New X-Men (2012) #1',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97170',
              name: 'cover from All-New X-Men (2012) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97171',
              name: 'story from All-New X-Men (2012) #1',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97172',
              name: 'cover from All-New X-Men (2012) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97173',
              name: 'story from All-New X-Men (2012) #1',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97174',
              name: 'cover from All-New X-Men (2012) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97175',
              name: 'story from All-New X-Men (2012) #1',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97176',
              name: 'A+X (2012) #2',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97177',
              name: 'story from All-New X-Men (2012) #1',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97182',
              name: 'A+X (2012) #3',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97183',
              name: 'story from All-New X-Men (2012) #3',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97186',
              name: 'cover from All-New X-Men (2012) #4',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97187',
              name: 'story from All-New X-Men (2012) #4',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97188',
              name: 'A+X (2012) #4',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97189',
              name: 'story from All-New X-Men (2012) #5',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97190',
              name: 'cover from All-New X-Men (2012) #6',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97191',
              name: 'story from All-New X-Men (2012) #6',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/97192',
              name: 'cover from All-New X-Men (2012) #7',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        comics: {
          available: 27,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/16450/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43488',
              name: 'A+X (2012) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/45763',
              name: 'A+X (2012) #1 (2nd Printing Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43495',
              name: 'A+X (2012) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43498',
              name: 'A+X (2012) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/44569',
              name: 'A+X (2012) #3 (Tan Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43499',
              name: 'A+X (2012) #3 (Mcguinness Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43500',
              name: 'A+X (2012) #3 (Mcguinness Sketch Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43501',
              name: 'A+X (2012) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43502',
              name: 'A+X (2012) #4 (Mcguinness Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43503',
              name: 'A+X (2012) #4 (Mcguinness Sketch Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/45765',
              name: 'A+X (2012) #4 (Brooks Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43504',
              name: 'A+X (2012) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/45766',
              name: 'A+X (2012) #5 (Del Mundo Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43505',
              name: 'A+X (2012) #6',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43506',
              name: 'A+X (2012) #7',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/47068',
              name: 'A+X (2012) #7 (Renaud Iron Man Many Armors Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43507',
              name: 'A+X (2012) #8',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43508',
              name: 'A+X (2012) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43509',
              name: 'A+X (2012) #10',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43510',
              name: 'A+X (2012) #11',
            },
          ],
          returned: 20,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/16450/events',
          items: [],
          returned: 0,
        },
        next: null,
        previous: null,
      },
      {
        id: 3614,
        title: 'Age of Apocalypse: The Chosen (1995)',
        description: null,
        resourceURI: 'http://gateway.marvel.com/v1/public/series/3614',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/series/3614/age_of_apocalypse_the_chosen_1995?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
        ],
        startYear: 1995,
        endYear: 1995,
        rating: '',
        type: '',
        modified: '2020-08-24T12:51:02-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/2/00/4bad2bdd3c8a9',
          extension: 'jpg',
        },
        creators: {
          available: 16,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/3614/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11063',
              name: 'Terry Kevin Austin',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/362',
              name: 'Scott Hanna',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/438',
              name: 'Karl Kesel',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/13392',
              name: 'Sergio Melia-Borras',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1914',
              name: 'Dan Panosian',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/844',
              name: 'James Pascoe',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/19',
              name: 'Tim Sale',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2030',
              name: 'Bob Wiacek',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1192',
              name: 'Ian Churchill',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11757',
              name: 'Salvador Larroca',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1163',
              name: 'Tom Lyle',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4523',
              name: 'Val Semeiks',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1249',
              name: 'Steve Skroce',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1286',
              name: 'Howard Mackie',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/350',
              name: 'Richard Starkings',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/5756',
              name: 'Ashley Underwood',
              role: 'colorist',
            },
          ],
          returned: 16,
        },
        characters: {
          available: 13,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/3614/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009159',
              name: 'Archangel',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009163',
              name: 'Aurora',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009182',
              name: 'Bishop',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009243',
              name: 'Colossus',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009257',
              name: 'Cyclops',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009337',
              name: 'Havok',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009447',
              name: 'Mister Sinister',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009476',
              name: 'Northstar',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009504',
              name: 'Professor X',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009524',
              name: 'Quicksilver',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009629',
              name: 'Storm',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009725',
              name: 'X-Man',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009726',
              name: 'X-Men',
            },
          ],
          returned: 13,
        },
        stories: {
          available: 24,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/3614/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37492',
              name: '',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37493',
              name: '[opening]',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37494',
              name: 'Magneto and Rogue',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37495',
              name: 'X-Man',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37496',
              name: 'Cyclops',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37497',
              name: 'Havok',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37498',
              name: 'Mr. Sinister',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37499',
              name: 'Storm',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37500',
              name: 'Quicksilver',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37501',
              name: 'Northstar and Aurora',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37502',
              name: 'Beast',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37503',
              name: 'The Four Horsemen',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37504',
              name: 'X-Calibre',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37505',
              name: 'The Human High Council',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37506',
              name: 'Weapon X and Jean Grey',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37507',
              name: 'Gambit and the Externals',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37508',
              name: 'Colossus',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37509',
              name: 'Angel',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37510',
              name: 'Sabretooth and Wildchild',
              type: '',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37511',
              name: 'Bishop',
              type: '',
            },
          ],
          returned: 20,
        },
        comics: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/3614/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17701',
              name: 'Age of Apocalypse: The Chosen (1995) #1',
            },
          ],
          returned: 1,
        },
        events: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/3614/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
          ],
          returned: 1,
        },
        next: null,
        previous: null,
      },
      {
        id: 13603,
        title: 'Age of X: Alpha (2010)',
        description: null,
        resourceURI: 'http://gateway.marvel.com/v1/public/series/13603',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/series/13603/age_of_x_alpha_2010?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
        ],
        startYear: 2010,
        endYear: 2010,
        rating: '',
        type: '',
        modified: '2015-03-02T15:52:24-0500',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/f0/54f4cd6d628c7',
          extension: 'jpg',
        },
        creators: {
          available: 14,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/13603/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/232',
              name: 'Chris Bachalo',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/412',
              name: 'Carlo Barberi',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/10107',
              name: 'Mirco Pierfederici',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/5251',
              name: 'Vc Joe Caramagna',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/366',
              name: 'Mike Carey',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/9224',
              name: 'Paul Davidson',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4150',
              name: 'Paco Diaz',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/427',
              name: 'Tim Townsend',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/10146',
              name: 'Gabriel Hernandez Walta',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4074',
              name: 'Walden Wong',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/428',
              name: 'Antonio Fabela',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1405',
              name: 'Matt Milla',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/582',
              name: 'Brian Reber',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11737',
              name: 'Daniel Ketchum',
              role: 'editor',
            },
          ],
          returned: 14,
        },
        characters: {
          available: 24,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/13603/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009158',
              name: 'Arcade',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009164',
              name: 'Avalanche',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009219',
              name: 'Cannonball',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009536',
              name: 'Cecilia Reyes',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009233',
              name: 'Chamber',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009243',
              name: 'Colossus',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009257',
              name: 'Cyclops',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009267',
              name: 'Dazzler',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1012478',
              name: 'Frenzy',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009313',
              name: 'Gambit',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1010678',
              name: 'Hellion',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009357',
              name: 'Husk',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009381',
              name: 'Jubilee',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009416',
              name: 'Magma (Amara Aquilla)',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009417',
              name: 'Magneto',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009465',
              name: 'Mystique',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009466',
              name: 'Namor',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009473',
              name: 'Nightmare',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1010692',
              name: 'Rockslide',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009546',
              name: 'Rogue',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 7,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/13603/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/83523',
              name: 'Age of X: Alpha (2010) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/83524',
              name: 'Interior #83524  ',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/85353',
              name: 'Age of X Alpha #1 2nd Printing ',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/89054',
              name: 'Age of X Alpha #1 ',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/89056',
              name: 'Age of X Alpha #1',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/89157',
              name: 'Age of X Alpha #1 Conrad 2nd Printing',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/89910',
              name: 'Cover #89910',
              type: 'cover',
            },
          ],
          returned: 7,
        },
        comics: {
          available: 3,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/13603/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/37996',
              name: 'Age of X: Alpha (2010) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/39284',
              name: 'Age of X: Alpha (2010) #1 (Conrad 2nd Printing Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38895',
              name: 'Age of X: Alpha (2010) #1 (2nd Printing Variant )',
            },
          ],
          returned: 3,
        },
        events: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/13603/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
          ],
          returned: 1,
        },
        next: null,
        previous: null,
      },
      {
        id: 2116,
        title: 'Alpha Flight (1983 - 1994)',
        description: null,
        resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/series/2116/alpha_flight_1983_-_1994?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
        ],
        startYear: 1983,
        endYear: 1994,
        rating: '',
        type: 'ongoing',
        modified: '2016-08-30T11:17:58-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/50/57c5a37acf5cc',
          extension: 'jpg',
        },
        creators: {
          available: 84,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/2116/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/3389',
              name: 'Ian Akin',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2707',
              name: 'Jeff Albrecht',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11063',
              name: 'Terry Kevin Austin',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2077',
              name: 'Hilary Barta',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4469',
              name: 'Richard Bennett',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/5823',
              name: 'Danilo Bulanadi',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/3074',
              name: 'Ralph Cabrera',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1865',
              name: 'Diana Albers',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1871',
              name: 'Ken Bruzenak',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/452',
              name: 'Virtual Calligr',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/87',
              name: 'Mark Bagley',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2720',
              name: 'Mike Bair',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1279',
              name: 'Jon Bogdanove',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/3394',
              name: 'Craig Brasfield',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1298',
              name: 'June Brigman',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1185',
              name: 'Pat Broderick',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/105',
              name: 'Sal Buscema',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/3415',
              name: 'John Calimee',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/3404',
              name: 'Dario Carrasco Jr.',
              role: 'penciler',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1827',
              name: 'John Byrne',
              role: 'penciller (cover)',
            },
          ],
          returned: 20,
        },
        characters: {
          available: 66,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/2116/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1010370',
              name: 'Alpha Flight',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009159',
              name: 'Archangel',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1011758',
              name: 'Attuma',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009163',
              name: 'Aurora',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009165',
              name: 'Avengers',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009168',
              name: 'Banshee',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009175',
              name: 'Beast',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009186',
              name: 'Black Knight (Sir Percy of Scandia)',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009206',
              name: 'Box',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009212',
              name: 'Bullseye',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009220',
              name: 'Captain America',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1010338',
              name: 'Captain Marvel (Carol Danvers)',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009243',
              name: 'Colossus',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009255',
              name: 'Crystal',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009257',
              name: 'Cyclops',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009262',
              name: 'Daredevil',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009267',
              name: 'Dazzler',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009281',
              name: 'Doctor Doom',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009282',
              name: 'Doctor Strange',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009306',
              name: 'Firestar',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 330,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/2116/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21096',
              name: 'Alpha Flight (1983) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21097',
              name: 'Tundra!',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21098',
              name: 'Cover #21098',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21099',
              name: 'Blood Battle',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21100',
              name: 'Family Ties',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21101',
              name: 'Cover #21101',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21102',
              name: 'The Final Option, Part Four: Decisions of Trust',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21103',
              name: 'Cover #21103',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21104',
              name: 'Death and How To Live It',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21105',
              name: 'Cover #21105',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21106',
              name: '...I Want To Live',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21107',
              name: 'Cover #21107',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21108',
              name: 'Spontaneous Combustion',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21109',
              name: 'Cover #21109',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21110',
              name: 'Headache',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21111',
              name: 'Cover #21111',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21112',
              name: 'The Bachelor Party',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21113',
              name: 'Cover #21113',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21114',
              name: 'The Walking Wounded',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/21115',
              name: 'World Tour Part 1',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        comics: {
          available: 130,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/2116/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12637',
              name: 'Alpha Flight (1983) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12679',
              name: 'Alpha Flight (1983) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12690',
              name: 'Alpha Flight (1983) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12701',
              name: 'Alpha Flight (1983) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12712',
              name: 'Alpha Flight (1983) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12723',
              name: 'Alpha Flight (1983) #6',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12734',
              name: 'Alpha Flight (1983) #7',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12745',
              name: 'Alpha Flight (1983) #8',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12756',
              name: 'Alpha Flight (1983) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12638',
              name: 'Alpha Flight (1983) #10',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12649',
              name: 'Alpha Flight (1983) #11',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12660',
              name: 'Alpha Flight (1983) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12671',
              name: 'Alpha Flight (1983) #13',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12673',
              name: 'Alpha Flight (1983) #14',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12674',
              name: 'Alpha Flight (1983) #15',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12675',
              name: 'Alpha Flight (1983) #16',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12676',
              name: 'Alpha Flight (1983) #17',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12677',
              name: 'Alpha Flight (1983) #18',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12678',
              name: 'Alpha Flight (1983) #19',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12680',
              name: 'Alpha Flight (1983) #20',
            },
          ],
          returned: 20,
        },
        events: {
          available: 3,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/2116/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/271',
              name: 'Secret Wars II',
            },
          ],
          returned: 3,
        },
        next: null,
        previous: null,
      },
      {
        id: 744,
        title: 'Astonishing X-Men (2004 - 2013)',
        description:
          "Marvel's mighty mutants go worldwide and beyond in this series following Cyclops, Wolverine, Beast, Emma Frost and more in their astonishing adventures!",
        resourceURI: 'http://gateway.marvel.com/v1/public/series/744',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/series/744/astonishing_x-men_2004_-_2013?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
        ],
        startYear: 2004,
        endYear: 2013,
        rating: 'T',
        type: 'ongoing',
        modified: '2015-08-27T12:54:49-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/9/30/5137a3b3d73ef',
          extension: 'jpg',
        },
        creators: {
          available: 68,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/744/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/8571',
              name: 'Guru-eFX',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12208',
              name: 'Arthur Adams',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/13215',
              name: 'Rain Beredo',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/648',
              name: 'Simone Bianchi',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1141',
              name: 'Giuseppe Camuncoli',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/162',
              name: 'John Cassaday',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/9937',
              name: 'Jim Charalampidis',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/10732',
              name: 'KNIGHT AGENCY, INC.',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/8658',
              name: 'James Asmus',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4014',
              name: 'Axel Alonso',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/937',
              name: 'Renato Arlem',
              role: 'artist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11798',
              name: 'Matteo Buffagni',
              role: 'artist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/13323',
              name: 'Carlos Cuevas',
              role: 'artist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/239',
              name: 'Juan Bobillo',
              role: 'penciller',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11875',
              name: 'Nick Bradshaw',
              role: 'penciller',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11543',
              name: 'Dan Brown',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/8504',
              name: "Frank D'ARMATA",
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/452',
              name: 'Virtual Calligr',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/5251',
              name: 'Vc Joe Caramagna',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/5127',
              name: 'Travis Charest',
              role: 'other',
            },
          ],
          returned: 20,
        },
        characters: {
          available: 45,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/744/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1011297',
              name: 'Agent Brand',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1010370',
              name: 'Alpha Flight',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009159',
              name: 'Archangel',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1011298',
              name: 'Armor (Hisako Ichiki)',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009165',
              name: 'Avengers',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009175',
              name: 'Beast',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009187',
              name: 'Black Panther',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009536',
              name: 'Cecilia Reyes',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009733',
              name: 'Charles Xavier',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009243',
              name: 'Colossus',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009257',
              name: 'Cyclops',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1010907',
              name: 'Dark Beast',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009267',
              name: 'Dazzler',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009310',
              name: 'Emma Frost',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009299',
              name: 'Fantastic Four',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009313',
              name: 'Gambit',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009340',
              name: 'Hellfire Club',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009343',
              name: 'Hercules',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009362',
              name: 'Iceman',
            },
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009368',
              name: 'Iron Man',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 198,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/744/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3305',
              name: 'Cover for Astonishing X-Men (2004) #7',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3306',
              name: '1 of 6 - Dangerous',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3307',
              name: 'Astonishing X-Men (2004) #3',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3308',
              name: 'Interior #3308',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3309',
              name: 'Astonishing X-Men (2004) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3310',
              name: 'Interior #3310',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3313',
              name: 'Cover #3313',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3314',
              name: 'Interior #3314',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3317',
              name: 'Astonishing X-Men (2004) #2',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3318',
              name: 'Interior #3318',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3319',
              name: 'Cover #3319',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3320',
              name: 'Interior #3320',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3321',
              name: 'Astonishing X-Men (2004) #5',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3322',
              name: '“GIFTED” 5 (OF 6)  As demand for the',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3323',
              name: 'Cover for Astonishing X-Men (2004) #6',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3324',
              name: '“GIFTED” PART 6 (OF 6) Outnumbered and outgunned, the X-Men are finally brought together as a team by their newest addition – bu',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3325',
              name: '2 of 6 - Dangerous',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3326',
              name: '2 of 6 - Dangerous',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3327',
              name: '3 of 6 - Dangerous',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3328',
              name: '3 of 6 - Dangerous',
              type: 'interiorStory',
            },
          ],
          returned: 20,
        },
        comics: {
          available: 85,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/744/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/660',
              name: 'Astonishing X-Men (2004) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/662',
              name: 'Astonishing X-Men (2004) #1 (Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/723',
              name: 'Astonishing X-Men (2004) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/531',
              name: 'Astonishing X-Men (2004) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1808',
              name: 'Astonishing X-Men (2004) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/766',
              name: 'Astonishing X-Men (2004) #4 (Foil Variant)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/843',
              name: 'Astonishing X-Men (2004) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/927',
              name: 'Astonishing X-Men (2004) #6',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38',
              name: 'Astonishing X-Men (2004) #7',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1436',
              name: 'Astonishing X-Men (2004) #8',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1626',
              name: 'Astonishing X-Men (2004) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1719',
              name: 'Astonishing X-Men (2004) #10',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/2842',
              name: 'Astonishing X-Men (2004) #11',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/2161',
              name: 'Astonishing X-Men (2004) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/3475',
              name: 'Astonishing X-Men (2004) #13',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4022',
              name: 'Astonishing X-Men (2004) #14',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4253',
              name: 'Astonishing X-Men (2004) #15',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4736',
              name: 'Astonishing X-Men (2004) #16',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/5034',
              name: 'Astonishing X-Men (2004) #17',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/5188',
              name: 'Astonishing X-Men (2004) #18',
            },
          ],
          returned: 20,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/744/events',
          items: [],
          returned: 0,
        },
        next: null,
        previous: null,
      },
    ],
  },
};
