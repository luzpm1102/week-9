export const mockedFilterCharacterByName = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '8ab4f2b559823ec09bf49f4e0c565ac048f5e454',
  data: {
    offset: 0,
    limit: 20,
    total: 4,
    count: 4,
    results: [
      {
        id: 1009629,
        name: 'Storm',
        description:
          'Ororo Monroe is the descendant of an ancient line of African priestesses, all of whom have white hair, blue eyes, and the potential to wield magic.',
        modified: '2016-05-26T11:50:27-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/40/526963dad214d',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009629',
        comics: {
          available: 852,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009629/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17701',
              name: 'Age of Apocalypse: The Chosen (1995) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43498',
              name: 'A+X (2012) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/37996',
              name: 'Age of X: Alpha (2010) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12676',
              name: 'Alpha Flight (1983) #17',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12694',
              name: 'Alpha Flight (1983) #33',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12725',
              name: 'Alpha Flight (1983) #61',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12668',
              name: 'Alpha Flight (1983) #127',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/21511',
              name: 'Astonishing X-Men (2004) #25',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/21714',
              name: 'Astonishing X-Men (2004) #26',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/21941',
              name: 'Astonishing X-Men (2004) #27',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/23087',
              name: 'Astonishing X-Men (2004) #28',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/23937',
              name: 'Astonishing X-Men (2004) #29',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24501',
              name: 'Astonishing X-Men (2004) #30',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24503',
              name: 'Astonishing X-Men (2004) #32',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24504',
              name: 'Astonishing X-Men (2004) #33',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24505',
              name: 'Astonishing X-Men (2004) #34',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/30332',
              name: 'Astonishing X-Men (2004) #35',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38318',
              name: 'Astonishing X-Men (2004) #38',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38319',
              name: 'Astonishing X-Men (2004) #40',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/39318',
              name: 'Astonishing X-Men (2004) #44',
            },
          ],
          returned: 20,
        },
        series: {
          available: 220,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009629/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/16450',
              name: 'A+X (2012 - 2014)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3614',
              name: 'Age of Apocalypse: The Chosen (1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13603',
              name: 'Age of X: Alpha (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
              name: 'Alpha Flight (1983 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/744',
              name: 'Astonishing X-Men (2004 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/31906',
              name: 'Atlantis Attacks: The Original Epic (2021)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1991',
              name: 'Avengers (1963 - 1996)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/354',
              name: 'Avengers (1998 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9085',
              name: 'Avengers (2010 - 2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1340',
              name: 'Avengers Assemble (2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/15305',
              name: 'Avengers Vs. X-Men (2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3626',
              name: 'Bishop (1994 - 1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/784',
              name: 'Black Panther (2005 - 2008)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/31553',
              name: 'Black Panther (2021 - Present)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/6804',
              name: 'Black Panther (2009 - 2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/20912',
              name: 'Black Panther (2016 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2115',
              name: 'Black Panther (1998 - 2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24291',
              name: 'Black Panther (2018 - 2021)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/23017',
              name: 'Black Panther and the Crew (2017)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3850',
              name: 'Black Panther Annual (2008)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 991,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009629/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/497',
              name: 'Interior #497',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/648',
              name: '1 of 2- Black Panther crossover',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/650',
              name: '2 of 2- Black Panther crossover',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/737',
              name: '2 of 5 - Savage Land',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/738',
              name: 'Uncanny X-Men (1963) #457',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/739',
              name: '3 of 5 - Savage Land',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/742',
              name: 'Uncanny X-Men (1963) #459',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/743',
              name: "5 of 5 - World's End",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/745',
              name: '1 of 2 - Mojo Rising',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/747',
              name: '2 of 2 - Mojo Rising',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/749',
              name: '1 of 4 - Season of the Witch',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/760',
              name: 'Uncanny X-Men (1963) #467',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/761',
              name: "2 of 3 - Grey's End",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/765',
              name: '1 of 3 -',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/767',
              name: "2 of 3 - Wand'ring Star",
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/768',
              name: 'UNCANNY X-MEN (1963) #471',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/770',
              name: 'UNCANNY X-MEN (1963) #472',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1420',
              name: 'ULTIMATE X-MEN (2000) #43',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1422',
              name: 'ULTIMATE X-MEN (2000) #44',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1426',
              name: 'ULTIMATE X-MEN (2000) #49',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        events: {
          available: 30,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009629/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/233',
              name: 'Atlantis Attacks',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/310',
              name: 'Avengers VS X-Men',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/238',
              name: 'Civil War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/318',
              name: 'Dark Reign',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/240',
              name: 'Days of Future Present',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/245',
              name: 'Enemy of the State',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/246',
              name: 'Evolutionary War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/249',
              name: 'Fatal Attractions',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/251',
              name: 'House of M',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/252',
              name: 'Inferno',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/334',
              name: 'Inhumans Vs. X-Men',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/255',
              name: 'Initiative',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/37',
              name: 'Maximum Security',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/299',
              name: 'Messiah CompleX',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/263',
              name: 'Mutant Massacre',
            },
          ],
          returned: 20,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1009629/storm?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Storm?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009629/storm?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1010979,
        name: 'Storm (Age of Apocalypse)',
        description:
          'Calling herself the Windrider, Storm carved out a small area of Africa as her own and intended to keep it safe from the strife of the war between humans and mutants.',
        modified: '2014-03-05T13:57:02-0500',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/00/5317732bcc91a',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010979',
        comics: {
          available: 12,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1010979/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/17735',
              name: 'Astonishing X-Men (1995) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/621',
              name: 'Official Handbook of the Marvel Universe (2004) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/700',
              name: 'Official Handbook of the Marvel Universe (2004) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/611',
              name: 'Official Handbook of the Marvel Universe (2004) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/565',
              name: 'Official Handbook of the Marvel Universe (2004) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1489',
              name: 'Official Handbook of the Marvel Universe (2004) #8',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1590',
              name: 'Official Handbook of the Marvel Universe (2004) #9 (THE WOMEN OF MARVEL)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1689',
              name: 'Official Handbook of the Marvel Universe (2004) #10 (MARVEL KNIGHTS)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1749',
              name: 'Official Handbook of the Marvel Universe (2004) #11 (X-MEN - AGE OF APOCALYPSE)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1886',
              name: 'Official Handbook of the Marvel Universe (2004) #12 (SPIDER-MAN)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/1994',
              name: 'Official Handbook of the Marvel Universe (2004) #13 (TEAMS)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4090',
              name: 'X-Men: The Complete Age of Apocalypse Epic Book 3 (Trade Paperback)',
            },
          ],
          returned: 12,
        },
        series: {
          available: 3,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1010979/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3619',
              name: 'Astonishing X-Men (1995)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/787',
              name: 'Official Handbook of the Marvel Universe (2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1684',
              name: 'X-Men: The Complete Age of Apocalypse Epic Book 3 (2006)',
            },
          ],
          returned: 3,
        },
        stories: {
          available: 11,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1010979/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3425',
              name: 'Cover #3425',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3588',
              name: 'Cover #3588',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3592',
              name: 'Cover #3592',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/3866',
              name: 'Cover #3866',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/4153',
              name: 'Cover #4153',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/4223',
              name: 'Cover #4223',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/4430',
              name: 'Cover #4430',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/4513',
              name: 'Cover #4513',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/4612',
              name: 'Cover #4612',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/4614',
              name: 'Cover #4614',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/37637',
              name: 'Once More With Feeling',
              type: '',
            },
          ],
          returned: 11,
        },
        events: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1010979/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/227',
              name: 'Age of Apocalypse',
            },
          ],
          returned: 1,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1010979/storm_age_of_apocalypse?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Storm_%28Age_of_Apocalypse%29?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1010979/storm_age_of_apocalypse?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1017309,
        name: 'Storm (Marvel Heroes)',
        description: '',
        modified: '2013-09-18T10:52:33-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/4/30/5239be29833f9',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1017309',
        comics: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1017309/comics',
          items: [],
          returned: 0,
        },
        series: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1017309/series',
          items: [],
          returned: 0,
        },
        stories: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1017309/stories',
          items: [],
          returned: 0,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1017309/events',
          items: [],
          returned: 0,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/characters/57/storm?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1017309/storm_marvel_heroes?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
      {
        id: 1010978,
        name: 'Storm (Ultimate)',
        description:
          'Ororo Munroe was raised in Morocco, learning English from American films, and after her family was murdered she fled to America, stealing cars in Harlem before relocating to Texas.',
        modified: '2014-03-05T13:58:04-0500',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/10/5317733a7ab7a',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1010978',
        comics: {
          available: 65,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1010978/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/46583',
              name: 'Ultimate Comics X-Men (2010) #23',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/4133',
              name: 'Ultimate Extinction (2006) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/13510',
              name: 'Ultimate Galactus Trilogy (Hardcover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15770',
              name: 'Ultimate Marvel Team-Up (2001) #11',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/5132',
              name: 'Ultimate Marvel Team-Up Ultimate Collection (Trade Paperback)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/18475',
              name: 'Ultimate War (2003) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/18476',
              name: 'Ultimate War (2003) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/18477',
              name: 'Ultimate War (2003) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15699',
              name: 'Ultimate X-Men (2001) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15710',
              name: 'Ultimate X-Men (2001) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15721',
              name: 'Ultimate X-Men (2001) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15732',
              name: 'Ultimate X-Men (2001) #4',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15743',
              name: 'Ultimate X-Men (2001) #5',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15754',
              name: 'Ultimate X-Men (2001) #6',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15765',
              name: 'Ultimate X-Men (2001) #7',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15767',
              name: 'Ultimate X-Men (2001) #8',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15768',
              name: 'Ultimate X-Men (2001) #9',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15700',
              name: 'Ultimate X-Men (2001) #10',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15701',
              name: 'Ultimate X-Men (2001) #11',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/15702',
              name: 'Ultimate X-Men (2001) #12',
            },
          ],
          returned: 20,
        },
        series: {
          available: 13,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1010978/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13108',
              name: 'Ultimate Comics X-Men (2010 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/759',
              name: 'Ultimate Extinction (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2223',
              name: 'Ultimate Galactus Trilogy (2007)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2311',
              name: 'Ultimate Marvel Team-Up (2001 - 2002)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1823',
              name: 'Ultimate Marvel Team-Up Ultimate Collection (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3659',
              name: 'Ultimate War (2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/474',
              name: 'Ultimate X-Men (2001 - 2009)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1055',
              name: 'Ultimate X-Men Annual (2005 - 2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13887',
              name: 'Ultimate X-Men MGC (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1662',
              name: 'Ultimate X-Men Vol. 14: Phoenix? (2006)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1168',
              name: 'ULTIMATE X-MEN VOL. 3: WORLD TOUR TPB (2005)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/216',
              name: 'ULTIMATE X-MEN VOL. 5: ULTIMATE WAR TPB (1999)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/82',
              name: 'Ultimate X-Men Vol. III: World Tour (2002)',
            },
          ],
          returned: 13,
        },
        stories: {
          available: 118,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1010978/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1450',
              name: '1 of 2 - Wolverine & Storm',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1454',
              name: 'Ultimate X-Men (2001) #61',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1455',
              name: '1 of 5 - Escape of Magneto',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1458',
              name: 'Ultimate X-Men (2001) #63',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1459',
              name: '3 of 5 - Magnetic North',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1462',
              name: 'Ultimate X-Men (2001) #65',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1463',
              name: '5 of 5 - Magnetic North',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1464',
              name: 'Ultimate X-Men (2001) #66',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1465',
              name: '1 of 3 -',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1466',
              name: 'Ultimate X-Men (2001) #67',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1467',
              name: '2 of 3 -',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1468',
              name: 'Ultimate X-Men (2001) #68',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1469',
              name: '3 of 3 - Date Night',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1470',
              name: 'Ultimate X-Men (2001) #69',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1471',
              name: '1 of 3 -',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1472',
              name: 'Ultimate X-Men (2001) #70',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1473',
              name: '2 of 3 -',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1886',
              name: 'Ultimate X-Men (2001) #39',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1887',
              name: 'Interior #1887',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/2041',
              name: 'Ultimate X-Men (2001) #40',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1010978/events',
          items: [],
          returned: 0,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/characters/57/storm?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Storm_%28Ultimate%29?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1010978/storm_ultimate?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
    ],
  },
};

export const mockedFilterByComic = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: 'a4bbac6259409cf83251f0c3790bfbf9e106c4d7',
  data: {
    offset: 0,
    limit: 5,
    total: 1,
    count: 1,
    results: [
      {
        id: 1009351,
        name: 'Hulk',
        description:
          'Caught in a gamma bomb explosion while trying to save the life of a teenager, Dr. Bruce Banner was transformed into the incredibly powerful creature called the Hulk. An all too often misunderstood hero, the angrier the Hulk gets, the stronger the Hulk gets.',
        modified: '2020-07-21T10:35:15-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009351',
        comics: {
          available: 1714,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/41112',
              name: '5 Ronin (Hardcover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/36365',
              name: '5 Ronin (2010) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38753',
              name: '5 Ronin (2010) #2 (BROOKS COVER)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43488',
              name: 'A+X (2012) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43506',
              name: 'A+X (2012) #7',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/77060',
              name: 'Absolute Carnage: Immortal Hulk (2019) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/320',
              name: 'Actor Presents Spider-Man and the Incredible Hulk (2003) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38524',
              name: 'Age of X: Universe (2011) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38523',
              name: 'Age of X: Universe (2011) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24053',
              name: 'All-New Savage She-Hulk (2009) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24252',
              name: 'All-New Savage She-Hulk (2009) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12689',
              name: 'Alpha Flight (1983) #29',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12650',
              name: 'Alpha Flight (1983) #110',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12651',
              name: 'Alpha Flight (1983) #111',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12668',
              name: 'Alpha Flight (1983) #127',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/35528',
              name: 'Amazing Spider-Man (1999) #667',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/16904',
              name: 'Amazing Spider-Man Annual (1964) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/16886',
              name: 'Amazing Spider-Man Annual (1964) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/36956',
              name: 'Amazing Spider-Man Annual (2011) #38',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67309',
              name: 'Ant-Man and the Wasp Adventures (Digest)',
            },
          ],
          returned: 20,
        },
        series: {
          available: 500,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/15276',
              name: '5 Ronin (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/12429',
              name: '5 Ronin (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/16450',
              name: 'A+X (2012 - 2014)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/27632',
              name: 'Absolute Carnage: Immortal Hulk (2019)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/458',
              name: 'Actor Presents Spider-Man and the Incredible Hulk (2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13896',
              name: 'Age of X: Universe (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/7231',
              name: 'All-New Savage She-Hulk (2009)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
              name: 'Alpha Flight (1983 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/454',
              name: 'Amazing Spider-Man (1999 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13205',
              name: 'Amazing Spider-Man Annual (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2984',
              name: 'Amazing Spider-Man Annual (1964 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24323',
              name: 'Ant-Man and the Wasp Adventures (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/22547',
              name: 'Avengers (2016 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9085',
              name: 'Avengers (2010 - 2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/354',
              name: 'Avengers (1998 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3621',
              name: 'Avengers (1996 - 1997)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1991',
              name: 'Avengers (1963 - 1996)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24044',
              name: 'Avengers & The Infinity Gauntlet (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9859',
              name: 'Avengers & the Infinity Gauntlet (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1988',
              name: 'Avengers Annual (1967 - 1994)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 2602,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/702',
              name: 'INCREDIBLE HULK (1999) #62',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/703',
              name: 'Interior #703',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/704',
              name: 'INCREDIBLE HULK (1999) #63',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/705',
              name: 'Interior #705',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/706',
              name: 'INCREDIBLE HULK (1999) #64',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/707',
              name: 'Interior #707',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/872',
              name: 'HULK: GRAY (2003) #2',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/873',
              name: 'Interior #873',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/874',
              name: 'HULK: GRAY (2003) #3',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/875',
              name: 'Interior #875',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1134',
              name: 'Interior #1134',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1217',
              name: 'INCREDIBLE HULK (1999) #68',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1218',
              name: 'Interior #1218',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1219',
              name: 'INCREDIBLE HULK (1999) #69',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1220',
              name: 'Interior #1220',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1221',
              name: 'INCREDIBLE HULK (1999) #70',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1222',
              name: 'Interior #1222',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1223',
              name: 'INCREDIBLE HULK (1999) #71',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1224',
              name: 'Interior #1224',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1225',
              name: 'INCREDIBLE HULK (1999) #75',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        events: {
          available: 26,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/296',
              name: 'Chaos War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/318',
              name: 'Dark Reign',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/297',
              name: 'Fall of the Hulks',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/302',
              name: 'Fear Itself',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/251',
              name: 'House of M',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/315',
              name: 'Infinity',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/317',
              name: 'Inhumanity',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/255',
              name: 'Initiative',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/311',
              name: 'Marvel NOW!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/37',
              name: 'Maximum Security',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/154',
              name: 'Onslaught',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/319',
              name: 'Original Sin',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/266',
              name: 'Other - Evolve or Die',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/212',
              name: 'Planet Hulk',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/295',
              name: 'Realm of Kings',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/269',
              name: 'Secret Invasion',
            },
          ],
          returned: 20,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1009351/hulk?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Hulk_(Bruce_Banner)?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009351/hulk?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
        ],
      },
    ],
  },
};

export const mockedFilterByStory = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '2f5cc32277c9c47f33d463c2829ba8b38b3bf398',
  data: {
    offset: 0,
    limit: 10,
    total: 1,
    count: 1,
    results: [
      {
        id: 1009351,
        name: 'Hulk',
        description:
          'Caught in a gamma bomb explosion while trying to save the life of a teenager, Dr. Bruce Banner was transformed into the incredibly powerful creature called the Hulk. An all too often misunderstood hero, the angrier the Hulk gets, the stronger the Hulk gets.',
        modified: '2020-07-21T10:35:15-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009351',
        comics: {
          available: 1714,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/41112',
              name: '5 Ronin (Hardcover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/36365',
              name: '5 Ronin (2010) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38753',
              name: '5 Ronin (2010) #2 (BROOKS COVER)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43488',
              name: 'A+X (2012) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43506',
              name: 'A+X (2012) #7',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/77060',
              name: 'Absolute Carnage: Immortal Hulk (2019) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/320',
              name: 'Actor Presents Spider-Man and the Incredible Hulk (2003) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38524',
              name: 'Age of X: Universe (2011) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38523',
              name: 'Age of X: Universe (2011) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24053',
              name: 'All-New Savage She-Hulk (2009) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24252',
              name: 'All-New Savage She-Hulk (2009) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12689',
              name: 'Alpha Flight (1983) #29',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12650',
              name: 'Alpha Flight (1983) #110',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12651',
              name: 'Alpha Flight (1983) #111',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12668',
              name: 'Alpha Flight (1983) #127',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/35528',
              name: 'Amazing Spider-Man (1999) #667',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/16904',
              name: 'Amazing Spider-Man Annual (1964) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/16886',
              name: 'Amazing Spider-Man Annual (1964) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/36956',
              name: 'Amazing Spider-Man Annual (2011) #38',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67309',
              name: 'Ant-Man and the Wasp Adventures (Digest)',
            },
          ],
          returned: 20,
        },
        series: {
          available: 500,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/12429',
              name: '5 Ronin (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/15276',
              name: '5 Ronin (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/16450',
              name: 'A+X (2012 - 2014)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/27632',
              name: 'Absolute Carnage: Immortal Hulk (2019)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/458',
              name: 'Actor Presents Spider-Man and the Incredible Hulk (2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13896',
              name: 'Age of X: Universe (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/7231',
              name: 'All-New Savage She-Hulk (2009)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
              name: 'Alpha Flight (1983 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/454',
              name: 'Amazing Spider-Man (1999 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2984',
              name: 'Amazing Spider-Man Annual (1964 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13205',
              name: 'Amazing Spider-Man Annual (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24323',
              name: 'Ant-Man and the Wasp Adventures (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/354',
              name: 'Avengers (1998 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9085',
              name: 'Avengers (2010 - 2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3621',
              name: 'Avengers (1996 - 1997)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/22547',
              name: 'Avengers (2016 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1991',
              name: 'Avengers (1963 - 1996)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9859',
              name: 'Avengers & the Infinity Gauntlet (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24044',
              name: 'Avengers & The Infinity Gauntlet (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1988',
              name: 'Avengers Annual (1967 - 1994)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 2602,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/702',
              name: 'INCREDIBLE HULK (1999) #62',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/703',
              name: 'Interior #703',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/704',
              name: 'INCREDIBLE HULK (1999) #63',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/705',
              name: 'Interior #705',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/706',
              name: 'INCREDIBLE HULK (1999) #64',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/707',
              name: 'Interior #707',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/872',
              name: 'HULK: GRAY (2003) #2',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/873',
              name: 'Interior #873',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/874',
              name: 'HULK: GRAY (2003) #3',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/875',
              name: 'Interior #875',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1134',
              name: 'Interior #1134',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1217',
              name: 'INCREDIBLE HULK (1999) #68',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1218',
              name: 'Interior #1218',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1219',
              name: 'INCREDIBLE HULK (1999) #69',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1220',
              name: 'Interior #1220',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1221',
              name: 'INCREDIBLE HULK (1999) #70',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1222',
              name: 'Interior #1222',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1223',
              name: 'INCREDIBLE HULK (1999) #71',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1224',
              name: 'Interior #1224',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1225',
              name: 'INCREDIBLE HULK (1999) #75',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        events: {
          available: 26,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/296',
              name: 'Chaos War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/318',
              name: 'Dark Reign',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/297',
              name: 'Fall of the Hulks',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/302',
              name: 'Fear Itself',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/251',
              name: 'House of M',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/315',
              name: 'Infinity',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/317',
              name: 'Inhumanity',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/255',
              name: 'Initiative',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/311',
              name: 'Marvel NOW!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/37',
              name: 'Maximum Security',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/154',
              name: 'Onslaught',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/319',
              name: 'Original Sin',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/266',
              name: 'Other - Evolve or Die',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/212',
              name: 'Planet Hulk',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/295',
              name: 'Realm of Kings',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/269',
              name: 'Secret Invasion',
            },
          ],
          returned: 20,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/characters/25/hulk?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Hulk_(Bruce_Banner)?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009351/hulk?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
      },
    ],
  },
};
