export const mockedFilterComicsByTitle = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: 'bdb22f21b111c05a187aa3ad95b8c1a842f31922',
  data: {
    offset: 0,
    limit: 5,
    total: 1887,
    count: 5,
    results: [
      {
        id: 95773,
        digitalId: 0,
        title: 'Avengers Forever (2021) #8',
        issueNumber: 8,
        variantDescription: '',
        description: null,
        modified: '2022-03-31T09:05:13-0400',
        isbn: '',
        upc: '75960620192100811',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 32,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/95773',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/95773/avengers_forever_2021_8?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/Avengers-Forever-8/digital-comic/59983?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/32867',
          name: 'Avengers Forever (2021 - Present)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2022-08-24T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2022-07-25T00:00:00-0400',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 3.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/d/03/6268464e4405f',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/d/03/6268464e4405f',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 7,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95773/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11463',
              name: 'Jason Aaron',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2133',
              name: 'Tom Brevoort',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12239',
              name: 'Guru Efx',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/426',
              name: 'Jason Keith',
              role: 'colorist (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12979',
              name: 'Aaron Kuder',
              role: 'penciler (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12980',
              name: 'Vc Cory Petit',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/480',
              name: 'Cam Smith',
              role: 'inker',
            },
          ],
          returned: 7,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95773/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95773/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/212610',
              name: 'cover from Avengers Forever (2021) #8',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/212611',
              name: 'story from Avengers Forever (2021) #8',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95773/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 99641,
        digitalId: 0,
        title: 'AVENGERS 1,000,000 BC 1 (2022) #1',
        issueNumber: 1,
        variantDescription: '',
        description: null,
        modified: '2022-03-31T09:05:07-0400',
        isbn: '',
        upc: '75960620320800111',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 40,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/99641',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/99641/avengers_1000000_bc_1_2022_1?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/AVENGERS-1-000-000-BC-1-1/digital-comic/59982?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/34340',
          name: 'AVENGERS 1,000,000 BC 1 (2022 - Present)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2022-08-17T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2022-07-18T00:00:00-0400',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 4.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/80/62684636a25bd',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/3/80/62684636a25bd',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 7,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/99641/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11463',
              name: 'Jason Aaron',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2133',
              name: 'Tom Brevoort',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/649',
              name: 'Ed Mcguinness',
              role: 'penciler (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12980',
              name: 'Vc Cory Petit',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/234',
              name: 'Kev Walker',
              role: 'inker',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/442',
              name: 'Dean White',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/10279',
              name: 'Matthew Wilson',
              role: 'colorist (cover)',
            },
          ],
          returned: 7,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/99641/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/99641/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/220382',
              name: 'cover #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/220383',
              name: 'story #1',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/99641/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 102926,
        digitalId: 0,
        title: 'AVENGERS & MOON GIRL 1 (2022) #1',
        issueNumber: 1,
        variantDescription: '',
        description: null,
        modified: '2022-03-31T09:05:05-0400',
        isbn: '',
        upc: '75960620444100111',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 40,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/102926',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/102926/avengers_moon_girl_1_2022_1?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/AVENGERS-MOON-GIRL-1-1/digital-comic/59981?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/35501',
          name: 'AVENGERS & MOON GIRL 1 (2022 - Present)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2022-07-27T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2022-06-27T00:00:00-0400',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 4.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/1/60/6268473c567e7',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/1/60/6268473c567e7',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 6,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/102926/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/14038',
              name: 'Lauren Bisom',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/430',
              name: 'Edgar Delgado',
              role: 'colorist (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12993',
              name: 'Vc Travis Lanham',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/13254',
              name: 'Alitha E. Martinez',
              role: 'penciler (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/14224',
              name: 'Mohale Mashigo',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/8382',
              name: 'Diogenes Neves',
              role: 'inker',
            },
          ],
          returned: 6,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/102926/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/102926/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/226955',
              name: 'cover from Avengers & Moon Girl: TBD (2022) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/226956',
              name: 'story from Avengers & Moon Girl: TBD (2022) #1',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/102926/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 95763,
        digitalId: 0,
        title: 'Avengers (2018) #58',
        issueNumber: 58,
        variantDescription: '',
        description: null,
        modified: '2022-03-31T09:05:02-0400',
        isbn: '',
        upc: '75960608857705811',
        diamondCode: '',
        ean: '',
        issn: '2644-1616',
        format: 'Comic',
        pageCount: 32,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/95763',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/95763/avengers_2018_58?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/Avengers-58/digital-comic/59980?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/24229',
          name: 'Avengers (2018 - Present)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2022-07-20T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2022-06-20T00:00:00-0400',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 3.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/90/6268463678646',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/90/6268463678646',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 5,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95763/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11463',
              name: 'Jason Aaron',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2133',
              name: 'Tom Brevoort',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/13021',
              name: 'David Curiel',
              role: 'colorist (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12426',
              name: 'Javier Garron',
              role: 'penciler (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12980',
              name: 'Vc Cory Petit',
              role: 'letterer',
            },
          ],
          returned: 5,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95763/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95763/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/212590',
              name: 'cover from Avengers (2018) #58',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/212591',
              name: 'story from Avengers (2018) #58',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95763/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 95772,
        digitalId: 0,
        title: 'Avengers Forever (2021) #7',
        issueNumber: 7,
        variantDescription: '',
        description: null,
        modified: '2022-02-26T09:05:35-0500',
        isbn: '',
        upc: '75960620192100711',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 32,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/95772',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/95772/avengers_forever_2021_7?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/Avengers-Forever-7/digital-comic/59589?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/32867',
          name: 'Avengers Forever (2021 - Present)',
        },
        variants: [
          {
            resourceURI: 'http://gateway.marvel.com/v1/public/comics/102603',
            name: 'Avengers Forever (2021) #7 (Variant)',
          },
        ],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2022-07-06T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2022-06-06T00:00:00-0400',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 3.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/c0/624f45f377f1b',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/6/c0/624f45f377f1b',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 7,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95772/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11463',
              name: 'Jason Aaron',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/2133',
              name: 'Tom Brevoort',
              role: 'editor',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12239',
              name: 'Guru Efx',
              role: 'colorist',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/426',
              name: 'Jason Keith',
              role: 'colorist (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12979',
              name: 'Aaron Kuder',
              role: 'penciler (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/12980',
              name: 'Vc Cory Petit',
              role: 'letterer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/480',
              name: 'Cam Smith',
              role: 'inker',
            },
          ],
          returned: 7,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95772/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95772/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/212608',
              name: 'cover from Avengers Forever (2021) #7',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/212609',
              name: 'story from Avengers Forever (2021) #7',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/95772/events',
          items: [],
          returned: 0,
        },
      },
    ],
  },
};

export const mockedFilterComicsByFormat = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '51865fcc96e489d51654613ea3c7ea8e792d0e2e',
  data: {
    offset: 0,
    limit: 5,
    total: 85,
    count: 5,
    results: [
      {
        id: 89889,
        digitalId: 0,
        title: 'EMPYRE MAGAZINE [BUNDLES OF 25] (2020) #1',
        issueNumber: 1,
        variantDescription: '',
        description: null,
        modified: '2020-05-28T09:04:17-0400',
        isbn: '',
        upc: '75960608479191911',
        diamondCode: 'JAN208551',
        ean: '',
        issn: '',
        format: 'Magazine',
        pageCount: 56,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/89889',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/89889/empyre_magazine_bundles_of_25_2020_1?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/EMPYRE-MAGAZINE-BUNDLES-OF-25-1/digital-comic/55021?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/30915',
          name: 'EMPYRE MAGAZINE [BUNDLES OF 25] (2020)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2020-07-01T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2020-03-09T00:00:00-0400',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 0,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/a/00/5e8653bcb7f38',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/a/00/5e8653bcb7f38',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/89889/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11896',
              name: 'Peter Charpentier',
              role: 'editor',
            },
          ],
          returned: 1,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/89889/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/89889/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/200096',
              name: 'cover from Empyre Magazine (2020) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/200097',
              name: 'story from Empyre Magazine (2020) #1',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/89889/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 76438,
        digitalId: 0,
        title: 'WAR OF THE REALMS MAGAZINE [BUNDLES OF 25] (2019) #1',
        issueNumber: 1,
        variantDescription: '',
        description: null,
        modified: '2019-03-25T09:25:48-0400',
        isbn: '',
        upc: '75960608479172011',
        diamondCode: 'DEC189150',
        ean: '',
        issn: '',
        format: 'Magazine',
        pageCount: 56,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/76438',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/76438/war_of_the_realms_magazine_bundles_of_25_2019_1?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'purchase',
            url: 'http://comicstore.marvel.com/WAR-OF-THE-REALMS-MAGAZINE-BUNDLES-OF-25-1/digital-comic/51480?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/27415',
          name: 'WAR OF THE REALMS MAGAZINE [BUNDLES OF 25] (2019)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2019-03-27T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2019-02-18T00:00:00-0500',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 0,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/80/5c8a4d109bf6f',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/c/80/5c8a4d109bf6f',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/76438/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/11896',
              name: 'Peter Charpentier',
              role: 'editor',
            },
          ],
          returned: 1,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/76438/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/76438/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/169742',
              name: 'War of Realms Magazine',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/169743',
              name: 'story from new series (2019) #1',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/76438/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 57395,
        digitalId: 41676,
        title: 'Captain America 75th Anniversary Magazine (2016) #1',
        issueNumber: 1,
        variantDescription: '',
        description:
          "Celebrate the birthday of a living legend with an All-New, All-Different, ALL-FREE commemoration of 75 years of Captain America! From fans' first impression of Steve Rogers socking evil in the jaw, to Sam Wilson wielding the shield today, this magazine has it all - bringing you fully up to date with Cap's comics and films! From a guide to the firmest friends and fiercest foes of Cap's storied history, to an in-depth look at his timeless costume, to features focusing on some of the finest creators ever to write or draw the Sentinel of Liberty - including John Romita Sr., Mark Gruenwald, Mark Waid and the King himself, Jack Kirby! Do your patriotic duty and take home this FREE slice of red, white and blue. Happy birthday, Captain - we salute you!",
        modified: '2016-04-07T16:44:58-0400',
        isbn: '978-0-7851-9904-5',
        upc: '75960608425800111',
        diamondCode: 'NOV158128',
        ean: '',
        issn: '',
        format: 'Magazine',
        pageCount: 96,
        textObjects: [
          {
            type: 'issue_solicit_text',
            language: 'en-us',
            text: "Celebrate the birthday of a living legend with an All-New, All-Different, ALL-FREE commemoration of 75 years of Captain America! From fans' first impression of Steve Rogers socking evil in the jaw, to Sam Wilson wielding the shield today, this magazine has it all - bringing you fully up to date with Cap's comics and films! From a guide to the firmest friends and fiercest foes of Cap's storied history, to an in-depth look at his timeless costume, to features focusing on some of the finest creators ever to write or draw the Sentinel of Liberty - including John Romita Sr., Mark Gruenwald, Mark Waid and the King himself, Jack Kirby! Do your patriotic duty and take home this FREE slice of red, white and blue. Happy birthday, Captain - we salute you!",
          },
        ],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/57395',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/57395/captain_america_75th_anniversary_magazine_2016_1?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
          {
            type: 'reader',
            url: 'http://marvel.com/digitalcomics/view.htm?iid=41676&utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/20913',
          name: 'Captain America 75th Anniversary Magazine (2016)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2016-04-13T00:00:00-0400',
          },
          {
            type: 'focDate',
            date: '2016-03-30T00:00:00-0400',
          },
          {
            type: 'unlimitedDate',
            date: '2016-10-10T00:00:00-0400',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 0,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/f0/5706b7326a322',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/f0/5706b7326a322',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 3,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/57395/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/1827',
              name: 'John Byrne',
              role: 'penciller (cover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/10363',
              name: 'John Rhett Thomas',
              role: 'writer',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/4430',
              name: 'Jeff Youngquist',
              role: 'editor',
            },
          ],
          returned: 3,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/57395/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/57395/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/125608',
              name: 'cover from Captain America 75th Anniversary Magazine (2016) #1',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/125609',
              name: 'story from Captain America 75th Anniversary Magazine (2016) #1',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/57395/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 39216,
        digitalId: 0,
        title: 'Disney/Pixar Giant Size Comics (2011) #8',
        issueNumber: 8,
        variantDescription: '',
        description: '',
        modified: '2011-12-27T01:31:29-0500',
        isbn: '',
        upc: '5960607640-00811',
        diamondCode: 'OCT110666',
        ean: '',
        issn: '2160-9470',
        format: 'Magazine',
        pageCount: 96,
        textObjects: [
          {
            type: 'issue_solicit_text',
            language: 'en-us',
            text: '',
          },
        ],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/39216',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/39216/disneypixar_giant_size_comics_2011_8?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/14181',
          name: 'Disney/Pixar Giant Size Comics (2011)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2011-12-28T00:00:00-0500',
          },
          {
            type: 'focDate',
            date: '2011-12-14T00:00:00-0500',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 5.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/4/80/4e7baa5abf0bc',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/4/80/4e7baa5abf0bc',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/39216/creators',
          items: [],
          returned: 0,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/39216/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/39216/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/89004',
              name: 'Cover #89004',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/89005',
              name: 'Interior #89005',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/39216/events',
          items: [],
          returned: 0,
        },
      },
      {
        id: 39217,
        digitalId: 0,
        title: 'Disney/Pixar Giant Size Comics (2011) #7',
        issueNumber: 7,
        variantDescription: '',
        description: null,
        modified: '2011-11-22T16:36:55-0500',
        isbn: '',
        upc: '5960607640-00711',
        diamondCode: 'SEP110573',
        ean: '',
        issn: '2160-9470',
        format: 'Magazine',
        pageCount: 96,
        textObjects: [],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/39217',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/39217/disneypixar_giant_size_comics_2011_7?utm_campaign=apiRef&utm_source=e92fdc433559d6b3161ce3114c29cccb',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/14181',
          name: 'Disney/Pixar Giant Size Comics (2011)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2011-11-30T00:00:00-0500',
          },
          {
            type: 'focDate',
            date: '2011-11-16T00:00:00-0500',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 5.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/f0/4ecbf7dab7489',
          extension: 'jpg',
        },
        images: [
          {
            path: 'http://i.annihil.us/u/prod/marvel/i/mg/f/f0/4ecbf7dab7489',
            extension: 'jpg',
          },
        ],
        creators: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/39217/creators',
          items: [],
          returned: 0,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/39217/characters',
          items: [],
          returned: 0,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/39217/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/89006',
              name: 'Cover #89006',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/89007',
              name: 'Interior #89007',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/39217/events',
          items: [],
          returned: 0,
        },
      },
    ],
  },
};
