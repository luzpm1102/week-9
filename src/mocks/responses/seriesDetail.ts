export const mockedSerieDetail = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '0df432ce7148442fa33441a1cb21ca5b0c10bcab',
  data: {
    offset: 0,
    limit: 5,
    total: 1,
    count: 1,
    results: [
      {
        id: 565,
        title: 'Startling Stories: The Incorrigible Hulk (2004)',
        description: null,
        resourceURI: 'http://gateway.marvel.com/v1/public/series/565',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/series/565/startling_stories_the_incorrigible_hulk_2004?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
        ],
        startYear: 2004,
        endYear: 2004,
        rating: 'MARVEL PSR',
        type: '',
        modified: '-0001-11-30T00:00:00-0500',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
          extension: 'jpg',
        },
        creators: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/565/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/6291',
              name: 'Peter Bagge',
              role: 'penciller (cover)',
            },
          ],
          returned: 1,
        },
        characters: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/565/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009351',
              name: 'Hulk',
            },
          ],
          returned: 1,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/565/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1891',
              name: 'Cover #1891',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1892',
              name: 'Interior #1892',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        comics: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/565/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/183',
              name: 'Startling Stories: The Incorrigible Hulk (2004) #1',
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/series/565/events',
          items: [],
          returned: 0,
        },
        next: null,
        previous: null,
      },
    ],
  },
};

export const mockedSeriesComics = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '0ab70c86e053b0dbc04711477f9affb1a4776e36',
  data: {
    offset: 0,
    limit: 5,
    total: 1,
    count: 1,
    results: [
      {
        id: 183,
        digitalId: 0,
        title: 'Startling Stories: The Incorrigible Hulk (2004) #1',
        issueNumber: 1,
        variantDescription: '',
        description: '',
        modified: '-0001-11-30T00:00:00-0500',
        isbn: '',
        upc: '5960605429-00811',
        diamondCode: '',
        ean: '',
        issn: '',
        format: 'Comic',
        pageCount: 0,
        textObjects: [
          {
            type: 'issue_solicit_text',
            language: 'en-us',
            text: 'For Doctor Bruce Banner life is anything but normal. But what happens when two women get between him and his alter ego, the Incorrigible Hulk? Hulk confused! \r\nIndy superstar Peter Bagge (THE MEGALOMANIACAL SPIDER-MAN) takes a satirical jab at the Hulk mythos with a tale of dames, debauchery and destruction.\r\n32 PGS./MARVEL PSR...$2.99',
          },
        ],
        resourceURI: 'http://gateway.marvel.com/v1/public/comics/183',
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/issue/183/startling_stories_the_incorrigible_hulk_2004_1?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
        ],
        series: {
          resourceURI: 'http://gateway.marvel.com/v1/public/series/565',
          name: 'Startling Stories: The Incorrigible Hulk (2004)',
        },
        variants: [],
        collections: [],
        collectedIssues: [],
        dates: [
          {
            type: 'onsaleDate',
            date: '2029-12-31T00:00:00-0500',
          },
          {
            type: 'focDate',
            date: '-0001-11-30T00:00:00-0500',
          },
        ],
        prices: [
          {
            type: 'printPrice',
            price: 2.99,
          },
        ],
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available',
          extension: 'jpg',
        },
        images: [],
        creators: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/183/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/6291',
              name: 'Peter Bagge',
              role: 'penciller (cover)',
            },
          ],
          returned: 1,
        },
        characters: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/183/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009351',
              name: 'Hulk',
            },
          ],
          returned: 1,
        },
        stories: {
          available: 2,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/183/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1891',
              name: 'Cover #1891',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1892',
              name: 'Interior #1892',
              type: 'interiorStory',
            },
          ],
          returned: 2,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/comics/183/events',
          items: [],
          returned: 0,
        },
      },
    ],
  },
};

export const mockedSeriesCharacters = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: 'a4bbac6259409cf83251f0c3790bfbf9e106c4d7',
  data: {
    offset: 0,
    limit: 5,
    total: 1,
    count: 1,
    results: [
      {
        id: 1009351,
        name: 'Hulk',
        description:
          'Caught in a gamma bomb explosion while trying to save the life of a teenager, Dr. Bruce Banner was transformed into the incredibly powerful creature called the Hulk. An all too often misunderstood hero, the angrier the Hulk gets, the stronger the Hulk gets.',
        modified: '2020-07-21T10:35:15-0400',
        thumbnail: {
          path: 'http://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0',
          extension: 'jpg',
        },
        resourceURI: 'http://gateway.marvel.com/v1/public/characters/1009351',
        comics: {
          available: 1714,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/41112',
              name: '5 Ronin (Hardcover)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/36365',
              name: '5 Ronin (2010) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38753',
              name: '5 Ronin (2010) #2 (BROOKS COVER)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43488',
              name: 'A+X (2012) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/43506',
              name: 'A+X (2012) #7',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/77060',
              name: 'Absolute Carnage: Immortal Hulk (2019) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/320',
              name: 'Actor Presents Spider-Man and the Incredible Hulk (2003) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38524',
              name: 'Age of X: Universe (2011) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/38523',
              name: 'Age of X: Universe (2011) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24053',
              name: 'All-New Savage She-Hulk (2009) #1',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/24252',
              name: 'All-New Savage She-Hulk (2009) #2',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12689',
              name: 'Alpha Flight (1983) #29',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12650',
              name: 'Alpha Flight (1983) #110',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12651',
              name: 'Alpha Flight (1983) #111',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/12668',
              name: 'Alpha Flight (1983) #127',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/35528',
              name: 'Amazing Spider-Man (1999) #667',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/16904',
              name: 'Amazing Spider-Man Annual (1964) #3',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/16886',
              name: 'Amazing Spider-Man Annual (1964) #12',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/36956',
              name: 'Amazing Spider-Man Annual (2011) #38',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/67309',
              name: 'Ant-Man and the Wasp Adventures (Digest)',
            },
          ],
          returned: 20,
        },
        series: {
          available: 500,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/15276',
              name: '5 Ronin (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/12429',
              name: '5 Ronin (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/16450',
              name: 'A+X (2012 - 2014)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/27632',
              name: 'Absolute Carnage: Immortal Hulk (2019)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/458',
              name: 'Actor Presents Spider-Man and the Incredible Hulk (2003)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13896',
              name: 'Age of X: Universe (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/7231',
              name: 'All-New Savage She-Hulk (2009)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2116',
              name: 'Alpha Flight (1983 - 1994)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/454',
              name: 'Amazing Spider-Man (1999 - 2013)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/13205',
              name: 'Amazing Spider-Man Annual (2011)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/2984',
              name: 'Amazing Spider-Man Annual (1964 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24323',
              name: 'Ant-Man and the Wasp Adventures (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/22547',
              name: 'Avengers (2016 - 2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9085',
              name: 'Avengers (2010 - 2012)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/354',
              name: 'Avengers (1998 - 2004)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/3621',
              name: 'Avengers (1996 - 1997)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1991',
              name: 'Avengers (1963 - 1996)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/24044',
              name: 'Avengers & The Infinity Gauntlet (2018)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/9859',
              name: 'Avengers & the Infinity Gauntlet (2010)',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/1988',
              name: 'Avengers Annual (1967 - 1994)',
            },
          ],
          returned: 20,
        },
        stories: {
          available: 2602,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/stories',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/702',
              name: 'INCREDIBLE HULK (1999) #62',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/703',
              name: 'Interior #703',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/704',
              name: 'INCREDIBLE HULK (1999) #63',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/705',
              name: 'Interior #705',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/706',
              name: 'INCREDIBLE HULK (1999) #64',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/707',
              name: 'Interior #707',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/872',
              name: 'HULK: GRAY (2003) #2',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/873',
              name: 'Interior #873',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/874',
              name: 'HULK: GRAY (2003) #3',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/875',
              name: 'Interior #875',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1134',
              name: 'Interior #1134',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1217',
              name: 'INCREDIBLE HULK (1999) #68',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1218',
              name: 'Interior #1218',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1219',
              name: 'INCREDIBLE HULK (1999) #69',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1220',
              name: 'Interior #1220',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1221',
              name: 'INCREDIBLE HULK (1999) #70',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1222',
              name: 'Interior #1222',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1223',
              name: 'INCREDIBLE HULK (1999) #71',
              type: 'cover',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1224',
              name: 'Interior #1224',
              type: 'interiorStory',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/stories/1225',
              name: 'INCREDIBLE HULK (1999) #75',
              type: 'cover',
            },
          ],
          returned: 20,
        },
        events: {
          available: 26,
          collectionURI:
            'http://gateway.marvel.com/v1/public/characters/1009351/events',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/116',
              name: 'Acts of Vengeance!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/303',
              name: 'Age of X',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/296',
              name: 'Chaos War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/318',
              name: 'Dark Reign',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/297',
              name: 'Fall of the Hulks',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/248',
              name: 'Fall of the Mutants',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/302',
              name: 'Fear Itself',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/251',
              name: 'House of M',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/315',
              name: 'Infinity',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/29',
              name: 'Infinity War',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/317',
              name: 'Inhumanity',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/255',
              name: 'Initiative',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/311',
              name: 'Marvel NOW!',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/37',
              name: 'Maximum Security',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/154',
              name: 'Onslaught',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/319',
              name: 'Original Sin',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/266',
              name: 'Other - Evolve or Die',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/212',
              name: 'Planet Hulk',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/295',
              name: 'Realm of Kings',
            },
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/events/269',
              name: 'Secret Invasion',
            },
          ],
          returned: 20,
        },
        urls: [
          {
            type: 'detail',
            url: 'http://marvel.com/comics/characters/1009351/hulk?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
          {
            type: 'wiki',
            url: 'http://marvel.com/universe/Hulk_(Bruce_Banner)?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
          {
            type: 'comiclink',
            url: 'http://marvel.com/comics/characters/1009351/hulk?utm_campaign=apiRef&utm_source=c0c99a36802a966230396c48505cd8f6',
          },
        ],
      },
    ],
  },
};

export const mockedSeriesStories = {
  code: 200,
  status: 'Ok',
  copyright: '© 2022 MARVEL',
  attributionText: 'Data provided by Marvel. © 2022 MARVEL',
  attributionHTML:
    '<a href="http://marvel.com">Data provided by Marvel. © 2022 MARVEL</a>',
  etag: '774e3182d09a1ddbf8307dcdb60e51c1b9bbf398',
  data: {
    offset: 0,
    limit: 5,
    total: 2,
    count: 2,
    results: [
      {
        id: 1891,
        title: 'Cover #1891',
        description: '',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/1891',
        type: 'cover',
        modified: '1969-12-31T19:00:00-0500',
        thumbnail: null,
        creators: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1891/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/6291',
              name: 'Peter Bagge',
              role: 'penciller (cover)',
            },
          ],
          returned: 1,
        },
        characters: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1891/characters',
          items: [],
          returned: 0,
        },
        series: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1891/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/565',
              name: 'Startling Stories: The Incorrigible Hulk (2004)',
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1891/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/183',
              name: 'Startling Stories: The Incorrigible Hulk (2004) #1',
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1891/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/183',
          name: 'Startling Stories: The Incorrigible Hulk (2004) #1',
        },
      },
      {
        id: 1892,
        title: 'Interior #1892',
        description: '',
        resourceURI: 'http://gateway.marvel.com/v1/public/stories/1892',
        type: 'story',
        modified: '1969-12-31T19:00:00-0500',
        thumbnail: null,
        creators: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1892/creators',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/creators/6291',
              name: 'Peter Bagge',
              role: 'writer',
            },
          ],
          returned: 1,
        },
        characters: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1892/characters',
          items: [
            {
              resourceURI:
                'http://gateway.marvel.com/v1/public/characters/1009351',
              name: 'Hulk',
            },
          ],
          returned: 1,
        },
        series: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1892/series',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/series/565',
              name: 'Startling Stories: The Incorrigible Hulk (2004)',
            },
          ],
          returned: 1,
        },
        comics: {
          available: 1,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1892/comics',
          items: [
            {
              resourceURI: 'http://gateway.marvel.com/v1/public/comics/183',
              name: 'Startling Stories: The Incorrigible Hulk (2004) #1',
            },
          ],
          returned: 1,
        },
        events: {
          available: 0,
          collectionURI:
            'http://gateway.marvel.com/v1/public/stories/1892/events',
          items: [],
          returned: 0,
        },
        originalIssue: {
          resourceURI: 'http://gateway.marvel.com/v1/public/comics/183',
          name: 'Startling Stories: The Incorrigible Hulk (2004) #1',
        },
      },
    ],
  },
};
