import { useState } from 'react';

export const usePagination = (limit: number) => {
  const [currentPage, setCurrentPage] = useState(1);
  const itemsPerPage = limit;
  const indexOfTheLastItem = currentPage * itemsPerPage;
  const indexOfTheFirstItem = indexOfTheLastItem - itemsPerPage;
  const paginate = (pageNumber: number) => setCurrentPage(pageNumber);
  return {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  };
};
