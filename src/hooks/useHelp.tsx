import React from 'react';
import axios, { AxiosResponse } from 'axios';
import { useDispatch } from 'react-redux';
import { ComicsResponse } from '../interfaces/Comics';
import { setComics } from '../redux/actions/comicsActions';

import { COMICS, STORIES } from '../helpers/constants';
import { StoriesResponse } from '../interfaces/Stories';
import { setStories } from '../redux/actions/storiesAction';

export const useHelp = () => {
  const dispatch = useDispatch();
  const getComics = async (query?: string) => {
    const result: void | AxiosResponse<ComicsResponse> = await axios
      .get(query ? COMICS + query : COMICS)
      .catch((err) => {
        console.log(err);
      });
    if (result) {
      dispatch(setComics(result.data.data.results));
    }
  };

  const getStories = async (query?: string) => {
    const result: void | AxiosResponse<StoriesResponse> = await axios
      .get(query ? STORIES + query : STORIES)
      .catch((err) => {
        console.log(err);
      });
    if (result) {
      dispatch(setStories(result.data.data.results));
    }
  };

  return { getComics, getStories };
};
