import { actionTypes, ActionTypes } from '../../helpers/constants';
import { Serie } from '../../interfaces/Series';

const initialState = {
  series: [],
};

type seriesActionType = {
  type: ActionTypes;
  payload: Serie[];
};

export const seriesReducer = (
  state = initialState,
  { type, payload }: seriesActionType
) => {
  switch (type) {
    case actionTypes.SET_SERIES_RELATED:
      return { ...state, seriesRelated: payload };
    case actionTypes.SET_SERIES:
      return { ...state, series: payload };
    case actionTypes.SET_SERIE:
      return { ...state, serie: payload };

    default:
      return state;
  }
};
