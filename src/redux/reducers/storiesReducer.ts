import { actionTypes, ActionTypes } from '../../helpers/constants';
import { Character } from '../../interfaces/Character';

const initialState = {
  stories: [],
};

type storiesActionType = {
  type: ActionTypes;
  payload: Character[];
};

export const storiesReducer = (
  state = initialState,
  { type, payload }: storiesActionType
) => {
  switch (type) {
    case actionTypes.SET_STORIES_RELATED:
      return { ...state, storiesRelated: payload };
    case actionTypes.SET_STORIES:
      const item = localStorage.getItem('hidden');

      const hidden: number[] = JSON.parse(item ? item : JSON.stringify([]));

      if (hidden.length > 0) {
        let newPayload = payload.filter((story) => !hidden.includes(story.id));

        return { ...state, stories: newPayload };
      }
      return { ...state, stories: payload };
    case actionTypes.SET_STORY:
      return { ...state, story: payload };

    default:
      return state;
  }
};
