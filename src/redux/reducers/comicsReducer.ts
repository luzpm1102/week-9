import { actionTypes, ActionTypes } from '../../helpers/constants';
import { Comic } from '../../interfaces/Comics';

type initialStateType = {
  comics: Comic[];
};
const initialState: initialStateType = {
  comics: [],
};
type comicsActionType = {
  type: ActionTypes;
  payload: Comic[];
};

export const comicsReducer = (
  state = initialState,
  { type, payload }: comicsActionType
) => {
  switch (type) {
    case actionTypes.SET_COMICS_RELATED:
      return { ...state, comicsRelated: payload };
    case actionTypes.SET_COMICS:
      const item = localStorage.getItem('hidden');

      const hidden: number[] = JSON.parse(item ? item : JSON.stringify([]));

      if (hidden.length > 0) {
        let newPayload = payload.filter((comic) => !hidden.includes(comic.id));

        return { ...state, comics: newPayload };
      }
      return { ...state, comics: payload };
    case actionTypes.SET_COMIC:
      return { ...state, comic: payload };

    default:
      return state;
  }
};
