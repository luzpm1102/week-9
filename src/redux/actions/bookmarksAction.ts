import { Bookmark } from '../../interfaces/Share';

export const setBookmark = (object: Bookmark) => {
  return {
    type: 'SET_BOOKMARK',
    payload: object,
  };
};
export const removeAll = () => {
  return {
    type: 'REMOVE_BOOKMARKS',
  };
};
export const removeOne = (id: number) => {
  return {
    type: 'REMOVE_BOOKMARK',
    payload: id,
  };
};
