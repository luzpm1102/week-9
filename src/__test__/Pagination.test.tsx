import ReactDOM from 'react-dom';
import { createRoot } from 'react-dom/client';
import { act } from 'react-dom/test-utils';
import Pagination from '../components/Pagination';

describe('Pagination render test', () => {
  test('Should render pagination', () => {
    const div = document.createElement('div');
    const root = createRoot(div);
    let currentPage = 1;

    act(() => {
      root.render(
        <Pagination
          totalpages={2}
          currentPage={currentPage}
          itemsPerPage={3}
          paginate={() => (currentPage = 2)}
        />
      );
    });

    expect(currentPage).toBe(1);
  });
});
