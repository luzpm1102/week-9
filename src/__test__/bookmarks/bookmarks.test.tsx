import { render, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import { combineReducers, legacy_createStore as createStore } from 'redux';
import { Bookmarks } from '../../views/Bookmarks';
import { bookmarkReducer } from '../../redux/reducers/bookmarkReducer';
import { Bookmark } from '../../interfaces/Share';
import store from '../../redux/store';
import userEvent from '@testing-library/user-event';

const initialState: Bookmark[] = [
  {
    type: 'character',
    id: 1,
    img: 'https://i.annihil.us/u/prod/marvel/i/mg/5/a0/538615ca33ab0.jpg',
    title: 'Hulk',
  },
  {
    type: 'serie',
    id: 2,
    img: '	https://i.annihil.us/u/prod/marvel/i/mg/f/20/4bc63a47b8dcb.jpg',
    title: 'OFFICIAL HANDBOOK OF THE MARVEL UNIVERSE (2004) #13 (TEAMS)',
  },
];

const reducer = combineReducers({
  allBookmarks: bookmarkReducer,
});

const fullStore = createStore(reducer, {
  allBookmarks: { bookmarks: initialState },
});

let testComponet = render(
  <Provider store={fullStore}>
    <MemoryRouter initialEntries={['/bookmarks']}>
      <Routes>
        <Route path='bookmarks' element={<Bookmarks />} />
      </Routes>
    </MemoryRouter>
  </Provider>
);

describe('Bookmarks test', () => {
  test('Bookmarks render wit full store', () => {
    expect(testComponet.getByText('Hulk')).toBeInTheDocument();
    expect(
      testComponet.getByText('OFFICIAL HANDBOOK OF THE MAR...')
    ).toBeInTheDocument();
  });
  test('Delete all button works', () => {
    const button = testComponet.queryByLabelText('delete-all');
    if (button) userEvent.click(button);
    expect(testComponet.queryByText('Hulk')).toBeFalsy();
  });

  test('Bookmarks empty', async () => {
    testComponet = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/bookmarks']}>
          <Routes>
            <Route path='bookmarks' element={<Bookmarks />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );
    expect(
      testComponet.getByText('NO BOOKMARKS ADDED YET')
    ).toBeInTheDocument();
  });
});
