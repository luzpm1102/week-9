import { render, RenderResult } from '@testing-library/react';
import { Provider } from 'react-redux';
import store from '../redux/store';
import { App } from '../App';

let testComponent: RenderResult;

beforeEach(() => {
  testComponent = render(
    <Provider store={store}>
      <App />
    </Provider>
  );
});
test('App render correctly', () => {
  expect(testComponent).toBeTruthy();
});
