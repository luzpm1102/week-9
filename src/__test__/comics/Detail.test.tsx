import { render, RenderResult, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import store from '../../redux/store';
import { ComicDetail } from '../../views/ComicDetail';
import { ComicsList } from '../../views/ComicsList';

let testComponent: RenderResult;

describe('Comic details tests', () => {
  beforeEach(async () => {
    testComponent = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/comic/1332']}>
          <Routes>
            <Route path='comics' element={<ComicsList />} />
            <Route path='comic/:id' element={<ComicDetail />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    await waitFor(() => {
      expect(testComponent.queryAllByLabelText(/loading/).length).toEqual(0);
    });
  });

  test('Comic detail is rendered', async () => {
    await waitFor(() => {
      const name = 'X-Men: Days of Future Past (Trade Paperback)';
      expect(testComponent.getByText(name.toUpperCase())).toBeInTheDocument();
    });
  });

  test('Save button works ', async () => {
    const button = testComponent.getByText('SAVE');
    userEvent.click(button);
    await waitFor(() => {
      testComponent.getByText('REMOVE');
    });
    expect(testComponent.getByText('REMOVE')).toBeDefined();
  });
  test('Hide button works ', async () => {
    const button = testComponent.getByText('HIDE');
    userEvent.click(button);
    await waitFor(() => {
      testComponent.getByText('Gun Theory (2003) #4');
    });
    expect(screen.getByText('Gun Theory (2003) #4')).toBeInTheDocument();
  });
});
