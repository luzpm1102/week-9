import { render, RenderResult, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import store from '../../redux/store';
import { ComicsList } from '../../views/ComicsList';

let testComponent: RenderResult;

beforeEach(async () => {
  testComponent = render(
    <Provider store={store}>
      <MemoryRouter initialEntries={['/comics']}>
        <Routes>
          <Route path='comics' element={<ComicsList />} />
        </Routes>
      </MemoryRouter>
    </Provider>
  );

  await waitFor(() => {
    expect(testComponent.queryByLabelText(/loading/)).toEqual(null);
  });
});

describe('Comic List tests', () => {
  test('Comic list render correctly', async () => {
    await waitFor(() => {
      expect(
        testComponent.getAllByText('Marvel Previews (2017)').length
      ).toBeGreaterThan(1);
      expect(
        testComponent.getByText('Gun Theory (2003) #4')
      ).toBeInTheDocument();
      expect(
        testComponent.getByText(
          'Startling Stories: The Incorrigible Hulk (2004) #1'
        )
      ).toBeInTheDocument();
    });
  });

  test('Local Storage get item is called', async () => {
    await waitFor(() => {
      expect(window.localStorage.getItem).toHaveBeenCalledTimes(4);
    });
  });
});
