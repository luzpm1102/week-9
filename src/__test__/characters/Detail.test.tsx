import {
  render,
  RenderResult,
  waitFor,
  screen,
  waitForElementToBeRemoved,
  cleanup,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import store from '../../redux/store';
import { CharacterDetails } from '../../views/CharacterDetails';
import userEvent from '@testing-library/user-event';
import { CharacterList } from '../../views/CharacterList';

let testComponent: RenderResult;

describe('Character details tests', () => {
  beforeEach(async () => {
    testComponent = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/characters/1009629']}>
          <Routes>
            <Route path='characters' element={<CharacterList />} />
            <Route path='characters/:id' element={<CharacterDetails />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    await waitFor(() => {
      expect(testComponent.queryAllByLabelText(/loading/).length).toEqual(0);
    });
  });

  test('Character detail is rendered', async () => {
    await waitFor(() => {
      expect(testComponent.getByText('STORM')).toBeInTheDocument();
    });
  });

  test('Save button works ', async () => {
    const button = testComponent.getByText('SAVE');
    userEvent.click(button);
    await waitFor(() => {
      testComponent.getByText('REMOVE');
    });
    expect(testComponent.getByText('REMOVE')).toBeDefined();
  });
  test('Hide button works ', async () => {
    const button = testComponent.getByText('HIDE');
    userEvent.click(button);
    await waitFor(() => {
      testComponent.getByText('3-D Man');
    });
    expect(screen.getByText('3-D Man')).toBeInTheDocument();
  });
});
