import { render, RenderResult, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import store from '../../redux/store';
import { CharacterList } from '../../views/CharacterList';

let testComponent: RenderResult;

describe('Character List tests', () => {
  beforeEach(async () => {
    testComponent = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/characters']}>
          <Routes>
            <Route path='characters' element={<CharacterList />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    await waitFor(() => {
      expect(testComponent.queryByLabelText(/loading/)).toEqual(null);
    });
  });

  test('Local Storage get item is called', async () => {
    await waitFor(() => {
      expect(window.localStorage.getItem).toHaveBeenCalledTimes(12);
    });
  });

  test('Character list render correctly', async () => {
    await waitFor(() => {
      expect(testComponent.getByText('3-D Man')).toBeInTheDocument();
      expect(testComponent.getByText('A-Bomb (HAS)')).toBeInTheDocument();
      expect(testComponent.getByText('A.I.M.')).toBeInTheDocument();
    });
  });
  test('Character search works correctly', async () => {
    const input = screen.getByRole('textbox');
    userEvent.type(input, 'storm');
    expect(input).toHaveValue('storm');
    expect(screen.queryByText('Storm')).toBeDefined();
  });
  test('Characters select', async () => {
    userEvent.selectOptions(
      screen.getByLabelText('select-comics'),
      'Startling Stories: The Incorrigible Hulk (2004) #1'
    );
    await waitFor(() => {
      expect(screen.queryByText('3-D Man')).toBeTruthy();
    });
  });

  test('Paginate is working', async () => {
    const pagination = screen.queryByLabelText('1');
    if (pagination) userEvent.click(pagination);
    expect(pagination).toHaveClass('pagination-list__link--active');
  });
});
