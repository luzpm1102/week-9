import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import store from '../redux/store';
import { Home } from '../views/Home';

let testComponent = render(
  <Provider store={store}>
    <MemoryRouter initialEntries={['/']}>
      <Routes>
        <Route path='/' element={<Home />} />
      </Routes>
    </MemoryRouter>
  </Provider>
);

test('should render Home page', () => {
  const info = testComponent.getByText('MARVEL INFORMATION');
  expect(info).toBeInTheDocument();
});
