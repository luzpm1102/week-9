import { render } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import store from '../redux/store';
import { Error } from '../views/Error';
import { Home } from '../views/Home';

let testComponent = render(
  <Provider store={store}>
    <MemoryRouter initialEntries={['/error']}>
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='/error' element={<Error />} />
      </Routes>
    </MemoryRouter>
  </Provider>
);

test('should render error page', () => {
  const notFound = testComponent.getByText('NOT FOUND');
  expect(notFound).toBeInTheDocument();
});
