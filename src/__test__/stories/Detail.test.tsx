import {
  render,
  RenderResult,
  waitFor,
  screen,
  waitForElementToBeRemoved,
  cleanup,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import store from '../../redux/store';
import userEvent from '@testing-library/user-event';
import { StoriesList } from '../../views/StoriesList';
import { StoryDetail } from '../../views/StoryDetail';

let testComponent: RenderResult;

describe('Story details tests', () => {
  beforeEach(async () => {
    testComponent = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/story/7']}>
          <Routes>
            <Route path='stories' element={<StoriesList />} />
            <Route path='story/:id' element={<StoryDetail />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    await waitFor(() => {
      expect(testComponent.queryAllByLabelText(/loading/).length).toEqual(0);
    });
  });

  test('Story detail is rendered', async () => {
    await waitFor(() => {
      const title =
        'Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf';
      expect(testComponent.getByText(title.toUpperCase())).toBeInTheDocument();
    });
  });

  test('Save button works ', async () => {
    const button = testComponent.getByText('SAVE');
    userEvent.click(button);
    await waitFor(() => {
      testComponent.getByText('REMOVE');
    });
    expect(testComponent.getByText('REMOVE')).toBeDefined();
  });
  test('Hide button works ', async () => {
    const button = testComponent.getByText('HIDE');
    userEvent.click(button);
    await waitFor(() => {
      testComponent.getByText('Interior #11...');
    });
    expect(screen.getByText('Interior #11...')).toBeInTheDocument();
  });
});
