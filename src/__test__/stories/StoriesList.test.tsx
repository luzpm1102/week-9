import { render, RenderResult, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import { ellipsify } from '../../helpers/constants';
import store from '../../redux/store';
import { StoriesList } from '../../views/StoriesList';

let testComponent: RenderResult;

beforeEach(async () => {
  testComponent = render(
    <Provider store={store}>
      <MemoryRouter initialEntries={['/stories']}>
        <Routes>
          <Route path='stories' element={<StoriesList />} />
        </Routes>
      </MemoryRouter>
    </Provider>
  );

  await waitFor(() => {
    expect(testComponent.queryByLabelText(/loading/)).toEqual(null);
  });
});

describe('Story List tests', () => {
  test('Story list render correctly', async () => {
    await waitFor(() => {
      const title =
        'Investigating the murder of a teenage girl, Cage suddenly learns that a three-way gang war is under way for control of the turf';
      expect(
        testComponent.getByText(ellipsify('Interior #11'))
      ).toBeInTheDocument();
      expect(testComponent.getByText(ellipsify(title))).toBeInTheDocument();
    });
  });

  test('Local Storage get item is called', async () => {
    await waitFor(() => {
      expect(window.localStorage.getItem).toHaveBeenCalledTimes(4);
    });
  });
});
