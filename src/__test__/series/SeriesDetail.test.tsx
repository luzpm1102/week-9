import { render, RenderResult, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import store from '../../redux/store';
import { SeriesDetail } from '../../views/SeriesDetail';
import { Home } from '../../views/Home';

let testComponent: RenderResult;

describe('Series details tests', () => {
  beforeEach(async () => {
    testComponent = render(
      <Provider store={store}>
        <MemoryRouter initialEntries={['/serie/565']}>
          <Routes>
            <Route path='serie/:id' element={<SeriesDetail />} />
            <Route path='/' element={<Home />} />
          </Routes>
        </MemoryRouter>
      </Provider>
    );

    await waitFor(() => {
      expect(testComponent.queryAllByLabelText(/loading/).length).toEqual(0);
    });
  });

  test('Serie detail is rendered', async () => {
    await waitFor(() => {
      const name = 'Hulk';
      expect(testComponent.getByText(name)).toBeInTheDocument();
    });
  });

  test('Save button works ', async () => {
    const button = testComponent.getByText('SAVE');
    userEvent.click(button);
    await waitFor(() => {
      testComponent.getByText('REMOVE');
    });
    expect(testComponent.getByText('REMOVE')).toBeDefined();
  });
});
