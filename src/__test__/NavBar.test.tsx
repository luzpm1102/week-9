import {
  fireEvent,
  render,
  RenderResult,
  screen,
} from '@testing-library/react';
import { Provider } from 'react-redux';
import { MemoryRouter, Route, Routes } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import { NavBar } from '../components/Navigation/NavBar';
import store from '../redux/store';
import { Home } from '../views/Home';

describe('Navbar Tests', () => {
  let testComponent: RenderResult;

  beforeEach(() => {
    testComponent = render(
      <Provider store={store}>
        <BrowserRouter>
          <NavBar />
        </BrowserRouter>
      </Provider>
    );
  });

  test('Link redirection to Characters exist', () => {
    const characters = testComponent.getByText('Characters');
    expect(characters).toHaveAttribute('href', '/characters');
    fireEvent.click(characters);
  });
  test('Link redirection to Comics exist', () => {
    const comics = testComponent.getByText('Comics');
    expect(comics).toHaveAttribute('href', '/comics');
    fireEvent.click(comics);
  });
  test('Link redirection to Stories exist', () => {
    const stories = testComponent.getByText('Stories');
    expect(stories).toHaveAttribute('href', '/stories');
    fireEvent.click(stories);
  });
  test('Link redirection to Bookmarks exist', () => {
    const bookmarks = testComponent.getByText('Bookmarks');
    expect(bookmarks).toHaveAttribute('href', '/bookmarks');
    fireEvent.click(bookmarks);
  });
});
