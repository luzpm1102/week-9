import { Character } from './Character';
import { Comic } from './Comics';
import { Serie } from './Series';
import { Story } from './Stories';

export interface Response {
  results: Character[] | Comic[] | Serie[] | Story[];
}

export interface stateInterface {
  allCharacters: {
    characterRelated: Character[];
    characters: Character[];
    character: Character;
  };
  allComics: {
    comicsRelated: Comic[];
    comics: Comic[];
    comic: Comic;
  };
  allSeries: {
    seriesRelated: Serie[];
    series: Serie[];
    serie: Serie;
  };
  allStories: {
    storiesRelated: Story[];
    stories: Story[];
    story: Story;
  };
  allBookmarks: {
    bookmarks: Bookmark[];
  };
}

export interface Bookmark {
  type: 'story' | 'comic' | 'serie' | 'character';
  id: number;
  img: string;
  title: string;
}
export interface Data {
  offset: number;
  limit: number;
  total: number;
  count: number;
  results: Character[] | Comic[] | Serie[] | Story[];
}

export interface Thumbnail {
  path: string;
  extension: Extension;
}

export enum Extension {
  GIF = 'gif',
  Jpg = 'jpg',
}

export interface URL {
  type: URLType;
  url: string;
}

export enum URLType {
  Comiclink = 'comiclink',
  Detail = 'detail',
  Wiki = 'wiki',
}

export interface Creators {
  available: number;
  collectionURI: string;
  items: CreatorsItem[];
  returned: number;
}

export interface CreatorsItem {
  resourceURI: string;
  name: string;
  role: string;
}
