import { Comics } from './Comics';
import { Thumbnail } from './Share';
import { Stories } from './Stories';

export interface CharactersResponse {
  code: number;
  status: string;
  copyright: string;
  attributionText: string;
  attributionHTML: string;
  etag: string;
  data: CharacterData;
}
export interface CharacterData {
  offset: number;
  limit: number;
  total: number;
  count: number;
  results: Character[];
}
export interface Character {
  id: number;
  name: string;
  description: string;
  modified: string;
  thumbnail: Thumbnail;
  resourceURI: string;
  comics: Comics;
  series: Comics;
  stories: Stories;
  events: Comics;
  urls: URL[];
}

export interface Characters {
  available: number;
  collectionURI: string;
  items: CharacterItem[];
  returned: number;
}

export interface CharacterItem {
  resourceURI: string;
  name: string;
}
