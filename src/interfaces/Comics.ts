import { Characters, CharacterItem } from './Character';
import { Creators, Thumbnail, URL } from './Share';
import { Stories } from './Stories';

export interface ComicsResponse {
  code: number;
  status: string;
  copyright: string;
  attributionText: string;
  attributionHTML: string;
  etag: string;
  data: ComicData;
}
export interface ComicData {
  offset: number;
  limit: number;
  total: number;
  count: number;
  results: Comic[];
}
export interface Comics {
  available: number;
  collectionURI: string;
  items: ComicsItem[];
  returned: number;
}

export interface ComicsItem {
  resourceURI: string;
  name: string;
}

export interface Comic {
  id: number;
  digitalId: number;
  title: string;
  issueNumber: number;
  variantDescription: string;
  description: null | string;
  modified: string;
  isbn: string;
  upc: string;
  diamondCode: DiamondCode;
  ean: string;
  issn: string;
  format: Format;
  pageCount: number;
  textObjects: TextObject[];
  resourceURI: string;
  urls: URL[];
  series: CharacterItem;
  variants: CharacterItem[];
  collections: any[];
  collectedIssues: CharacterItem[];
  dates: DateElement[];
  prices: Price[];
  thumbnail: Thumbnail;
  images: Thumbnail[];
  creators: Creators;
  characters: Characters;
  stories: Stories;
  events: Characters;
}

export enum Role {
  Colorist = 'colorist',
  Editor = 'editor',
  Inker = 'inker',
  Letterer = 'letterer',
  Penciler = 'penciler',
  Penciller = 'penciller',
  PencillerCover = 'penciller (cover)',
  Writer = 'writer',
}

export interface DateElement {
  type: DateType;
  date: string;
}

export enum DateType {
  FocDate = 'focDate',
  OnsaleDate = 'onsaleDate',
}

export enum DiamondCode {
  Empty = '',
  Jul190068 = 'JUL190068',
}

export enum Format {
  Comic = 'Comic',
  Digest = 'Digest',
  Empty = '',
  TradePaperback = 'Trade Paperback',
}

export interface Price {
  type: PriceType;
  price: number;
}

export enum PriceType {
  PrintPrice = 'printPrice',
}

export interface TextObject {
  type: TextObjectType;
  language: Language;
  text: string;
}

export enum Language {
  EnUs = 'en-us',
}

export enum TextObjectType {
  IssueSolicitText = 'issue_solicit_text',
}
