import React, { useEffect, useState } from 'react';
import { Character } from '../../interfaces/Character';
import { addFinalPart } from '../../helpers/constants';
import { ComicsRelated } from '../comics/ComicsRelated';
import { SeriesRelated } from '../series/seriesRelated';
import { StoriesRelated } from '../stories/storiesRelated';
import { useNavigate } from 'react-router-dom';
import { Bookmark, stateInterface } from '../../interfaces/Share';
import { useDispatch, useSelector } from 'react-redux';
import { setBookmark, removeOne } from '../../redux/actions/bookmarksAction';
interface Props {
  character: Character;
}

export const SingleCharacter = ({ character }: Props) => {
  const { name, description, comics, thumbnail, series, stories, id } =
    character;
  const comicsRelatedURI = addFinalPart(comics.collectionURI);
  const seriesRelatedURI = addFinalPart(series.collectionURI);
  const storyRelatedURI = addFinalPart(stories.collectionURI);
  const [favorite, setFavorite] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const hide = () => {
    const item = localStorage.getItem('hidden');
    const hidden: number[] = JSON.parse(item ? item : JSON.stringify([]));
    const copy = [...hidden, id];
    localStorage.setItem('hidden', JSON.stringify(copy));
    navigate('/characters');
  };

  useEffect(() => {
    if (bookmarks.includes(id)) {
      setFavorite(true);
    }
  }, []);

  const bookmarks = useSelector((state: stateInterface) =>
    state.allBookmarks.bookmarks.map((item) => item.id)
  );

  const deleteBookmark = () => {
    setFavorite(false);
    dispatch(removeOne(id));
  };

  const addToBookmark = () => {
    const bookmark: Bookmark = {
      id: id,
      type: 'character',
      img: `${thumbnail.path}.${thumbnail.extension}`,
      title: name,
    };
    setFavorite(true);
    dispatch(setBookmark(bookmark));
  };

  return (
    <div className='flex-container'>
      <div className='character-info'>
        <div className='image-container'>
          <img
            src={`${thumbnail.path}.${thumbnail.extension}`}
            alt={thumbnail.path}
            className='character__img'
          />
        </div>
        <div className='info-container'>
          <p className='info__item__title'> {name.toUpperCase()}</p>
          <h3 className='info__item'>Description:</h3>
          <p>{description ? description : 'No description'}</p>
          <div className='buttons-container'>
            <button onClick={hide} className='home-button'>
              HIDE
            </button>
            <button
              onClick={favorite ? deleteBookmark : addToBookmark}
              className='home-button'
            >
              {favorite ? 'REMOVE' : 'SAVE'}
            </button>
          </div>
        </div>
      </div>

      <ComicsRelated uri={comicsRelatedURI} />
      <SeriesRelated uri={seriesRelatedURI} />
      <StoriesRelated uri={storyRelatedURI} />
    </div>
  );
};
