import React, { useState } from 'react';
import { debounce } from 'lodash';
import { useNavigate } from 'react-router-dom';
import '../../styles/filter.scss';

interface Props {
  getComics: (query?: string) => Promise<void>;
}

const Filter = ({ getComics }: Props) => {
  const url = new URL(window.location.href);
  const [query, setQuery] = useState<string>('');
  const navigate = useNavigate();
  const params = url.search;

  const setTitle = (value: string) => {
    url.searchParams.set('titleStartsWith', value);
    navigate(`${url.search}`);
  };
  const getTitle = debounce(setTitle, 900);

  const setFormat = (value: string) => {
    url.searchParams.set('format', value);
    navigate(`${url.search}`);
  };

  if (params) {
    const title = url.searchParams.get('titleStartsWith');
    const format = url.searchParams.get('format');
    const searchQuery = `${title ? `&titleStartsWith=${title}` : ''}${
      format ? `&format=${format}` : ''
    }`;
    if (query !== searchQuery) {
      setQuery(searchQuery);
      getComics(searchQuery);
    }
  }

  return (
    <div className='filter'>
      <div className='filter-box'>
        <label htmlFor='title' className='filter__label'>
          Title
          <input
            className='filter__input'
            type='text'
            name='title'
            id='title'
            placeholder='title'
            onChange={(e) => getTitle(e.target.value)}
          />
        </label>
      </div>
      <div className='filter-box'>
        <label htmlFor='format' className='filter__label'>
          Format
          <select
            className='filter__input'
            name='format'
            id='format'
            onChange={(e) => setFormat(e.target.value)}
          >
            <option value=''>All</option>
            <option value='comic'>Comic</option>
            <option value='magazine'>Magazine</option>
            <option value='trade paperback'>Trade Paperback</option>
            <option value='hardcover'>Hardcover</option>
            <option value='digest'>Digest</option>
            <option value='graphic novel'>Graphic Novel</option>
            <option value='digital comic'>Digital Comic</option>
            <option value='infinite comic'>Infinite Comic</option>
          </select>
        </label>
      </div>
    </div>
  );
};

export default Filter;
