import React from 'react';
import '../styles/general.scss';
export const Loading = () => {
  return (
    <div className='loading-container'>
      <div className='loading' aria-label='loading'></div>
    </div>
  );
};
