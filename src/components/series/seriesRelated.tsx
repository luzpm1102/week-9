import axios, { AxiosResponse } from 'axios';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { stateInterface } from '../../interfaces/Share';
import { Card } from '../Card';
import { Loading } from '../Loading';
import { SeriesResponse } from '../../interfaces/Series';
import { setSeriesRelated } from '../../redux/actions/seriesAction';
import { usePagination } from '../../hooks/usePagination';
import Pagination from '../Pagination';
import { Link } from 'react-router-dom';
interface Props {
  uri: string;
}

export const SeriesRelated = ({ uri }: Props) => {
  const seriesRelated = useSelector(
    (state: stateInterface) => state.allSeries.seriesRelated
  );
  const [loading, setLoading] = useState(true);
  const dispatch = useDispatch();

  const getSeries = async () => {
    const result: void | AxiosResponse<SeriesResponse> = await axios
      .get(uri)
      .catch((err) => console.log(err));
    if (result) {
      dispatch(setSeriesRelated(result.data.data.results));
      setLoading(false);
    }
  };

  useEffect(() => {
    getSeries();
  }, []);

  const itemsLimit = 3;

  const {
    currentPage,
    itemsPerPage,
    indexOfTheFirstItem,
    indexOfTheLastItem,
    paginate,
  } = usePagination(itemsLimit);

  const display = seriesRelated
    ? seriesRelated
        .slice(indexOfTheFirstItem, indexOfTheLastItem)
        .map((serie) => (
          <Link to={`/serie/${serie.id}`} key={serie.id}>
            <Card
              img={`${serie.thumbnail.path}.${serie.thumbnail.extension}`}
              title={serie.title}
            />
          </Link>
        ))
    : [];

  return (
    <>
      <h3 className='center'>Series related</h3>
      {loading && <Loading />}
      <div className='related-container'>{seriesRelated && display}</div>
      <div>
        {seriesRelated && seriesRelated.length <= 0 ? (
          <h4>No series related</h4>
        ) : (
          <Pagination
            itemsPerPage={itemsPerPage}
            totalpages={seriesRelated ? seriesRelated.length : 0}
            paginate={paginate}
            currentPage={currentPage}
          />
        )}
      </div>
    </>
  );
};
