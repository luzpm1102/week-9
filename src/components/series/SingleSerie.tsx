import React, { useEffect, useState } from 'react';
import { addFinalPart } from '../../helpers/constants';
import { CharacterRelated } from '../characters/CharacterRelated';
import { Serie } from '../../interfaces/Series';
import { ComicsRelated } from '../comics/ComicsRelated';
import { StoriesRelated } from '../stories/storiesRelated';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Bookmark, stateInterface } from '../../interfaces/Share';
import { removeOne, setBookmark } from '../../redux/actions/bookmarksAction';

interface Props {
  serie: Serie;
}
export const SingleSerie = ({ serie }: Props) => {
  const { thumbnail, description, title, characters, comics, stories, id } =
    serie;
  const characterRelatedURI = addFinalPart(characters.collectionURI);
  const comicsRelatedURI = addFinalPart(comics.collectionURI);
  const storyRelatedURI = addFinalPart(stories.collectionURI);
  const [favorite, setFavorite] = useState(false);
  const dispatch = useDispatch();

  const addToBookmark = () => {
    const bookmark: Bookmark = {
      id: id,
      type: 'serie',
      img: `${thumbnail.path}.${thumbnail.extension}`,
      title: title,
    };
    setFavorite(true);
    dispatch(setBookmark(bookmark));
  };

  useEffect(() => {
    if (bookmarks.includes(id)) {
      setFavorite(true);
    }
  }, []);

  const bookmarks = useSelector((state: stateInterface) =>
    state.allBookmarks.bookmarks.map((item) => item.id)
  );

  const deleteBookmark = () => {
    setFavorite(false);
    dispatch(removeOne(id));
  };

  return (
    <div className='flex-container'>
      <div className='character-info'>
        <div className='image-container'>
          <img
            src={`${thumbnail.path}.${thumbnail.extension}`}
            alt={thumbnail.path}
            className='character__img'
          />
        </div>
        <div className='info-container'>
          <p className='info__item__title'> {title.toUpperCase()}</p>
          <h3 className='info__item'>Description:</h3>
          <p>{description ? description : 'No description'}</p>
          <div className='buttons-container'>
            <button
              onClick={favorite ? deleteBookmark : addToBookmark}
              className='home-button'
            >
              {favorite ? 'REMOVE' : 'SAVE'}
            </button>
          </div>
        </div>
      </div>
      <CharacterRelated uri={characterRelatedURI} />
      <ComicsRelated uri={comicsRelatedURI} />
      <StoriesRelated uri={storyRelatedURI} />
    </div>
  );
};
