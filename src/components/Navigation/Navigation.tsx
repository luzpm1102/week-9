import React from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import { Error } from '../../views/Error';
import { Home } from '../../views/Home';
import { CharacterList } from '../../views/CharacterList';
import { CharacterDetails } from '../../views/CharacterDetails';
import { NavBar } from './NavBar';
import { ComicsList } from '../../views/ComicsList';
import { ComicDetail } from '../../views/ComicDetail';
import { SeriesDetail } from '../../views/SeriesDetail';
import { StoriesList } from '../../views/StoriesList';
import { StoryDetail } from '../../views/StoryDetail';
import { Bookmarks } from '../../views/Bookmarks';

export enum paths {
  home = '/',
  error = '/error',
  characters = '/characters',
  comics = '/comics',
  stories = '/stories',
  OneCharacter = '/character/:id',
  OneComic = '/comic/:id',
  OneSerie = '/serie/:id',
  OneStory = '/story/:id',
  bookmarks = '/bookmarks',
}

export const Navigation = () => {
  return (
    <BrowserRouter>
      <NavBar />
      <Routes>
        <Route path={paths.home} element={<Home />} />
        <Route path={paths.characters} element={<CharacterList />} />
        <Route path={paths.comics} element={<ComicsList />} />
        <Route path={paths.stories} element={<StoriesList />} />
        <Route path={paths.OneCharacter} element={<CharacterDetails />} />
        <Route path={paths.OneComic} element={<ComicDetail />} />
        <Route path={paths.OneStory} element={<StoryDetail />} />
        <Route path={paths.OneSerie} element={<SeriesDetail />} />
        <Route path={paths.bookmarks} element={<Bookmarks />} />
        <Route path={paths.error} element={<Error />} />
        <Route path='*' element={<Navigate replace to={paths.error} />} />
      </Routes>
    </BrowserRouter>
  );
};
