import React from 'react';
import { Navigation } from './components/Navigation/Navigation';

export const App = () => {
  return (
    <>
      <Navigation />
    </>
  );
};
